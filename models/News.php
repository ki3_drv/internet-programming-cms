<?php

class News
{
  protected static $table = 'news';
  
  
  public static function createNews($options) {//+
    return DB::Create('news', $options);
  }

  public static function updateNewsById($id, $options) {//+
    return DB::Update('news', $options, ['id' => $id]);
  }

  public static function deleteNewsById($id) {
    return DB::Delete('news', ['id' => $id]);
  }



  public static function getNewsListAdmin() {//+
      return DB::Select('news', '');
  }

  public static function getNewsById($id) {//+
    return DB::Select('news', ['id' => $id])[0];
  }

  

  public static function getNewsList($offset = 0, $order = "id DESC") {//++
    $fields = 'id, title, description, dateposted';

    return DB::Select('news', null, $fields, ['limit'=>"$offset, 10", 'order'=>$order]);
  }

  public static function getLastNews($count = 10) { //--
      $params = array('order'=>'dateposted DESC', 'limit'=>'0, '.$count);

      return DB::Select('news', null, 'id, title, description, dateposted',
                         $params);
  }
  

  public static function getStatusText($status) {//+
    if ($status == 1) 
      return 'Відображається';
    return 'Не відображається';
  }

  public static function watch($id, $watches) {
    DB::Update(self::$table, ['watches' => ++$watches], ['id'=>$id]);
  }
}
