<?php
  class Photos {
    protected static $table = 'albums';

    public static function createAlbum($options) {
      return DB::Create(self::$table, $options);
    }

    public static function updateAlbumById($id, $options) {
      DB::Update(self::$table, $options, ['id'=>$id]);
    }

    public static function deleteAlbumById($id) {
        return DB::Delete(self::$table, ['id' => $id]);
    }



    public static function getAlbumsListAdmin() {
        return DB::Select(self::$table, null, '*', ['order'=>'dateposted DESC']);
    }

    public static function getAlbumsList($offset = 0, $order = 'id DESC', $count = 10) {
        $fields = 'id, title, dateposted, watches';
        $params = ['order' => $order, 'limit'=>"$offset, $count"];
        return DB::Select(self::$table, ['status'=>1], $fields, $params);
    }

    public static function getAlbumById($id) {
        return DB::Select(self::$table, ['id' => $id])[0];
    }

    public static function getAlbumNameById($id) {
      return DB::Select(self::$table, ['id' => $id], 'title')[0]['title'];
    }

    public static function getLastAlbumsId() {
        $fields = 'id';
        $params = ['order' => 'id DESC'];
        return DB::Select(self::$table, ['status'=>1], $fields, $params);
    }

    public static function getStatusText($status) {//+
        if ($status == 1) 
          return 'Відображається';
        return 'Не відображається';
      }

    public static function watch($id, $watches) {
    DB::Update(self::$table, ['watches' => ++$watches], ['id'=>$id]);
    }
  }