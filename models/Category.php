<?php
    class Category {
        private static $table = 'category';

        public static function getCategoryList() {
            return DB::Select(self::$table);
        }

        public static function getCategoryById($id) {
            return DB::Select(self::$table, ['id'=>$id])[0];
        }

        public static function getCategoryNameById($id) {
            $name = DB::Select(self::$table, ['id'=>$id], 'name');
            return !empty($name) ? $name[0]['name'] : false;
        }

        public static function deleteCategoryById($id) {
            return DB::Delete(self::$table, ['id'=>$id]);
        }

        public static function updateCategoryById($id, $options) {
            return DB::Update(self::$table, $options, ['id'=>$id]);
        }

        public static function createCategory($options) {
            return DB::Create(self::$table, $options);
        }
    }