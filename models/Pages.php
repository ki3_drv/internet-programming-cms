<?php
    class Pages {
        private static $table = 'pages';

        public static function getPagesList() {
            return DB::Select(self::$table, ['status'=>1]);
        }

        public static function getPagesListAdmin() {
            return DB::Select(self::$table);
        }

        public static function getPageById($id) {
            return DB::Select(self::$table, ['id'=>$id])[0];
        }

        public static function getPagesByCategoryId($category_id) {
            return DB::Select(self::$table, ['category_id'=>$category_id]);
        }

        public static function deletePageById($id) {
            return DB::Delete(self::$table, ['id'=>$id]);
        }

        public static function updatePageById($id, $options) {
            return DB::Update(self::$table, $options, ['id'=>$id]);
        }

        public static function createPage($options) {
            return DB::Create(self::$table, $options);
        }

        public static function disablePagesByCategoryId($id) {
            return DB::Update(self::$table,
                             ['category_id'=>0, 'status'=>0],
                              ['category_id'=>$id]);
        }

        public static function getStatusText($status) {
            return ($status !== '1') ? 'Не відображається' : 'Відображається';
        }

        public static function watch($id, $watches) {
            DB::Update(self::$table, ['watches' => ++$watches], ['id'=>$id]);
          }
    }