<?php
    class Users {
        private static $table = 'user';

        public static function userExists($login) {
            return !empty(DB::Select(self::$table, ['login'=>$login]));
        }

        public static function passwordIsCorrect($login, $passw) {
            return !empty(DB::Select(self::$table,
                         ['login'=>$login, 'password'=>md5($passw)]));
        }
    }