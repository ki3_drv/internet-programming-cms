<?php
    class Links {
        private static $table = 'links';

        public static function getLinksList() {
            return DB::Select(self::$table, ['status'=>1]);
        }

        public static function getLinksListAdmin() {
            return DB::Select(self::$table);
        }

        public static function getLinkById($id) {
            return DB::Select(self::$table, ['id'=>$id])[0];
        }

        public static function deleteLinkById($id) {
            return DB::Delete(self::$table, ['id'=>$id]);
        }

        public static function updateLinkById($id, $options) {
            return DB::Update(self::$table, $options, ['id'=>$id]);
        }

        public static function createLink($options) {
            return DB::Create(self::$table, $options);
        }

        public static function getStatusText($status) {
            return ($status !== '1') ? 'Не відображається' : 'Відображається';
        }
    }