<?php
    class Schedule {
        private static $table = 'schedule';

        public static function getScheduleList() {
            return DB::Select(self::$table, ['status'=>1]);
        }

        public static function getScheduleListAdmin() {
            return DB::Select(self::$table);
        }

        public static function classExists($class) {
            $class = DB::Select(self::$table, ['class'=>$class]);

            return !empty($class);
        }

        public static function getScheduleById($id) {
            return DB::Select(self::$table, ['id'=>$id])[0];
        }

        public static function deleteScheduleById($id) {
            return DB::Delete(self::$table, ['id'=>$id]);
        }

        public static function updateScheduleById($id, $options) {
            return DB::Update(self::$table, $options, ['id'=>$id]);
        }

        public static function createSchedule($options) {
            return DB::Create(self::$table, $options);
        }

        public static function getStatusText($status) {
            return ($status !== '1') ? 'Не відображається' : 'Відображається';
        }
    }