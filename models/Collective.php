<?php
  class Collective {

    private static $table = 'person';

    public static function createPersonInfo($options, $teacher = null) {
      $id = DB::Create(self::$table, $options);

      if (!empty($teacher)) {
        $teacher['person_id'] = $id;
        DB::Create('teacher', $teacher);
      }

      return $id;
    }

    public static function updatePersonInfo($id, $options, $teacher = null) {
      DB::Update(self::$table, $options, ['id'=>$id]);
      if (!empty($teacher)) {
        DB::Update('teacher', $teacher, ['person_id'=>$id]);
      }
    }

    public static function deletePersonInfoById($id) {
      DB::Delete(self::$table, ['id' => $id]);
      DB::Delete('teacher', ['person_id'=>$id]);
    }

    public static function getPeopleList() {
      return DB::Select(self::$table);
    }

    public static function getTeacherInfoByPersonId($id) {
      return DB::Select('teacher', ['person_id'=>$id])[0];
    }

    public static function getPersonById($id) {
      return DB::Select(self::$table, ['id'=>$id])[0];
    }

    public static function getTeachersList() {
      return DB::Select(self::$table, ['is_teacher'=>1]);
    }

    public static function getServicePeople() {
      return DB::Select(self::$table, ['is_teacher'=>0]);
    }

    public static function getAdmins() {
      return DB::Select(self::$table, ['is_admin'=>1]);
    }
  }