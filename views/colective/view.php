<?php require_once(ROOT . '/views/layouts/header.php'); ?>

<main>
  <div id="content">
    <section>
      <h3><?= $branch ?></h3>
      <div class="content">
        <?php if(!empty($people)): ?>
        <?php foreach($people as $person): ?>
        <div class="person">
          <!--<div class="photo">-->
            <img src="/public/img/foto.jpg" alt="">
          
          
            <h4><?php echo $person['l_name'].' '.$person['f_name'].' '.$person['m_name']; ?></h4>
            <p>Посада: <?= $person['post'] ?></p>

            <?php if ($person['is_teacher']): ?>
            <p>Освіта: <?= $person['education'] ?></p>
            <p>Спеціалізація: <?= $person['speciality'] ?></p>
            <p>Категорія: <?= $person['cathegory'] ?></p>
            <?php endif; ?>

            <p><?= $person['biography'] ?></p>
          
        </div>
        <?php endforeach; ?>
        <?php else: ?>
        <h2>Інформація відсутня</h2>
        <?php endif; ?>
        <hr>
      </div>
    </section>
  </div>

  <?php require_once(ROOT . '/views/layouts/aside.php'); ?>

</main>

<?php require_once(ROOT . '/views/layouts/footer.php'); ?>