<?php foreach($albums as $album): ?>
<div class="wrapper">
  <div class="album">
    <a href="/photos/view/<?= $album['id'] ?>"> <?= $album['title'] ?> </a>
    <div class="inner">
      <?php foreach($photos[$album['id']] as $photo): ?>
      <img src="/uploads/images/albums/<?= $album['id'].'/'.$photo ?>" alt="photo">
      <?php endforeach; ?>

    </div>
  </div>
  <hr>
  <div class="date-view-info">
    <span>Переглядів: <?=$album['watches']?></span>
    <span>Дата публікації: <?=$album['dateposted']?></span>
  </div>
</div>
<?php endforeach; ?>