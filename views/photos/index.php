<?php require_once(ROOT . '/views/layouts/header.php'); ?>
<main>
  <div id="content">
    <section>
      <h3><?= $title ?></h3>
      <div class="content">
        <?php foreach($albums as $album): ?>


        <div class="wrapper">


          <div class="album">
            <a href="/photos/view/<?= $album['id'] ?>"> <?= $album['title'] ?> </a>

            <div class="inner">
              <?php foreach($photos[$album['id']] as $photo): ?>
              <img src="/uploads/images/albums/<?= $album['id'].'/'.$photo ?>" alt="photo">
              <?php endforeach; ?>

            </div>
          </div>


          <hr>
          <div class="date-view-info">
            <span>Переглядів: <?=$album['watches']?></span>
            <span>Дата публікації: <?=$album['dateposted']?></span>
          </div>


        </div>
        <?php endforeach; ?>

      </div>
      <div class="loading spiner hidden">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
      <?php if(count($albums) > 9): ?>
      <button class="btn_more" onClick="loadAlbums();">Більше альбомів</button>
      <?php endif; ?>
    </section>
  </div>

  <?php require_once(ROOT . '/views/layouts/aside.php'); ?>

</main>
<hr>
<script src="/public/js/main.js"></script>

<?php require_once(ROOT . '/views/layouts/footer.php'); ?>