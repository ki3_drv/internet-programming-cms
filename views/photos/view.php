<?php require_once(ROOT . '/views/layouts/header.php'); ?>

<main>
  <div id="content">
    <section>
      <h3><?= $album['title'] ?></h3>
      <div class="content">
        <div class="photos album">
          <?php foreach($photos as $photo): ?>
          <img src="/uploads/images/albums/<?= $album['id'].'/'.$photo ?>" alt="photo">
          <?php endforeach; ?>
        </div>
        <hr>
        <div class="date-view-info">
        <span>Переглядів: <?= $album['watches'] ?></span>
        <span>Дата публікації: <?= $album['dateposted'] ?></span>
        </div>
      </div>
    </section>
  </div>

  <?php require_once(ROOT . '/views/layouts/aside.php'); ?>

</main>

<?php require_once(ROOT . '/views/layouts/footer.php'); ?>