<?php require_once(ROOT . '/views/layouts/header.php'); ?>
<main>
  <div id="content">
    <section>
      <h3><?= $page['title'] ?></h3>
      <div class="content">
        <article>
          <?= $page['content'] ?>
        </article>

        <hr>
        <div class="date-view-info">
          <span>Переглядів: <?= $page['watches'] ?></span>
          <span>Дата публікації: <?= $page['dateposted'] ?></span>
        </div>
      </div>
    </section>
  </div>
  <?php require_once(ROOT . '/views/layouts/aside.php'); ?>

</main>
<?php require_once(ROOT . '/views/layouts/footer.php'); ?>