<?php require_once ROOT . '/views/layouts/header.php'; ?>

<main>
  <div id="content">

    <script type="text/javascript" src="/public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/public/js/jquery.cycle2.min.js"></script>
    <script type="text/javascript" src="/public/js/jquery.cycle2.carousel.min.js"></script>

    <section id="slider">
      <div class="cycle-slideshow" data-cycle-fx=carousel data-cucle-pause-on-hover=true data-cycle-timeout=5000
        data-cycle-carousel-visible=1 data-cycle-carousel-fluid=true data-cycle-prev="#prev" data-cycle-next="#next">
        <?php foreach ($photos as $photo_path): ?>
        <img src="<?= $photo_path ?>" alt="slider_photo" />
        <?php endforeach; ?>
      </div>
      <a id="prev" href=""><i class="fa fa-angle-left"></i></a>
      <a id="next" href=""><i class="fa fa-angle-right"></i></a>
      <div class="controls">
      </div>
    </section>

    <section id="news">
      <h3>Новини</h3>
      <div class="content">

        <?php foreach($news as $item): ?>
        <div class="news_item">
          <?php if(file_exists('./uploads/images/news/'.$item['id'].'.jpg')): ?>
          <img src="<?= '/uploads/images/news/'.$item['id'].'.jpg' ?>" alt="news">
          <?php endif ?>
          <h4><?= $item['title'] ?></h4>
          <p><?= $item['description'] ?></p>
          <span><?= $item['dateposted'] ?></span>
          <a href="/news/view/<?= $item['id'] ?>">Докладніше</a>

        </div>
        <hr>
        <?php endforeach; ?>

      </div>
      <div class="loading spiner hidden">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
      <?php if(count($news) > 9): ?>
      <button class="btn_more" onClick="loadNews();">Більше новин</button>
      <?php endif; ?>
    </section>
  </div>
  <?php require_once ROOT . '/views/layouts/aside.php'; ?>
</main>
<hr>

<script src="/public/js/main.js"></script>



<?php require_once ROOT . '/views/layouts/footer.php'; ?>