<?php require_once(ROOT . '/views/layouts/header.php'); ?>
<main>
  <div id="content">
    <section>
      <h3><?= $page['title'] ?></h3>
      <div class="content">
        <article>
          <?php if (file_exists("./uploads/images/page/{$id}.jpg")): ?>
          <div class="photos">
            <img src="/uploads/images/page/<?= $id ?>.jpg" alt="page photo">
          </div>
          <?php endif; ?>
          <?= $page['content'] ?>
        </article>

        <hr>
        <div class="date-view-info">
          <span>Переглядів: <?= $page['watches'] ?></span>
          <span>Дата публікації: <?= $page['dateposted'] ?></span>
        </div>
      </div>
    </section>
  </div>
  <?php require_once(ROOT . '/views/layouts/aside.php'); ?>

</main>
<?php require_once(ROOT . '/views/layouts/footer.php'); ?>