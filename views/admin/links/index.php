<?php require_once(ROOT . '/views/layouts/header_admin.php'); ?>

<section>
  <div class="container">
    <div class="row">

      <br />

      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="/admin">Адмінпанель</a></li>
          <li class="active">Управління посиланнями</li>
        </ol>
      </div>

      <a href="/admin/links/create" class="btn btn-default back">
        <i class="fa fa-plus"></i> Додати нове посилання
      </a>

      <h4>Посилання</h4>

      <br />

      <?php if (!empty($linksList)) : ?>
      <table class="table-bordered table-striped table w-100">
        <tr>
          <th>ID</th>
          <th>Назва</th>
          <th>Адреса посилання</th>
          <th>Статус</th>
          <th></th>
          <th></th>

        </tr>
        <?php foreach ($linksList as $link): ?>
        <tr>
          <td><?= $link['id'] ?></td>
          <td><?= $link['title'] ?></td>
          <td><?= $link['url'] ?></td>
          <td><?= Links::getStatusText($link['status']) ?></td>

          <td>
            <a href="/admin/links/update/<?= $link['id']; ?>" title="Редагувати">
              <i class="fa fa-pencil-square-o"></i>
            </a>
          </td>

          <td>
            <a href="/admin/links/delete/<?= $link['id']; ?>" title="Видалити">
              <i class="fa fa-times"></i>
            </a>
          </td>

        </tr>
        <?php endforeach; ?>
      </table>
      <?php else: ?>
      <p>Посилання відсутні</p>
      <?php endif; ?>

    </div>
  </div>
</section>

<?php require_once(ROOT . '/views/layouts/footer_admin.php'); ?>