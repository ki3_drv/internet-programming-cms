<?php require_once(ROOT . '/views/layouts/header_admin.php'); ?>

<section>
  <div class="container">
    <div class="row">

      <br />

      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="/admin">Адмінпанель</a></li>
          <li><a href="/admin/links">Управління посиланнями</a></li>
          <li class="active"><?= $title ?></li>
        </ol>
      </div>

      <h4><?= $title ?></h4>

      <br />

      <?php if (isset($errors) && is_array($errors)): ?>
      <ul>
        <?php foreach ($errors as $error): ?>
        <li> - <?php echo $error; ?></li>
        <?php endforeach; ?>
      </ul>
      <?php endif; ?>

      <div class="col-lg-6">
        <div class="login-form">
          <form action="#" method="post" enctype="multipart/form-data">

            <p>Назва</p>
            <input type="text" name="title" value="<?= $link['title'] ?>">

            <p>Адреса посилання (https://site.com)</p>
            <input type="text" name="url" value="<?= $link['url'] ?>">

            <p>Статус</p>
            <select name="status">
              <option value="1" <?php echo ($link['status']==1)? 'selected':''; ?>>Відображається</option>
              <option value="0" <?php echo ($link['status']==0)? 'selected':''; ?>>Не відображається</option>
            </select>

            <br /><br />

            <input type="submit" name="submit" class="btn btn-default" value="Зберегти" multiple>

          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<?php require_once(ROOT . '/views/layouts/footer_admin.php'); ?>