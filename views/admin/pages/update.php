<?php require_once(ROOT . '/views/layouts/header_admin.php'); ?>

<section>
  <div class="container">
    <div class="row">

      <br />

      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="/admin">Адмінпанель</a></li>
          <li><a href="/admin/pages">Управління сторінками</a></li>
          <li class="active"><?= $title ?></li>
        </ol>
      </div>

      <h4><?= $title ?></h4>

      <br />

      <?php if (isset($errors) && is_array($errors)): ?>
      <ul>
        <?php foreach ($errors as $error): ?>
        <li> - <?php echo $error; ?></li>
        <?php endforeach; ?>
      </ul>
      <?php endif; ?>

      <div class="col-lg-6">
        <div class="login-form">
          <form action="#" method="post" enctype="multipart/form-data">

            <p>Заголовок сторінки</p>
            <input type="text" name="title" value="<?= $page['title'] ?>">


            <p>Зміст сторінки</p>
            <textarea name="content" rows="20"><?= $page['content'] ?></textarea>

            <p>Категорія</p>
            <select name="category_id">

              <?php if (!empty($categoryList)): ?>
              <option value="0">----- Обрати категорію -----</option>
              <?php foreach ($categoryList as $category): ?>
              <option value="<?= $category['id'] ?>" 
                <?php if($category['id'] === $page['category_id']) echo 'selected' ?>>

                <?= $category['name'] ?>

              </option>
              <?php endforeach; ?>
              <?php endif; ?>
            </select>

            <br /><br />
            <?php if (file_exists("./uploads/images/page/{$id}.jpg")): ?>
            <div id="upload-images">
              <img class="img" src="/uploads/images/page/<?= $id ?>.jpg" alt="page_image">
              <a href="/admin/pages/delete-image/<?= $id ?>" title="Видалити">
                <i class="fa fa-times"></i>
              </a>
            </div>      
            <?php endif; ?>
            <br />
            <div class="preview"></div>
            <p>Нове зображення сторінки</p>
            <input type="file" name="image" class="img" onInput="bindEvent();">


            <br /><br />

            <p>Статус</p>
            <select name="status">
              <option value="1" <?php echo ($page['status']==1)? 'selected':''; ?>>Відображається</option>
              <option value="0" <?php echo ($page['status']==0)? 'selected':''; ?>>Не відображається</option>
            </select>

            <br /><br />

            <input type="submit" name="submit" class="btn btn-default" value="Зберегти">

            <br /><br />

          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<?php require_once(ROOT . '/views/layouts/footer_admin.php'); ?>