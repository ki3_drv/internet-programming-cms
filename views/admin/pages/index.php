<?php require_once(ROOT . '/views/layouts/header_admin.php'); ?>

<section>
  <div class="container">
    <div class="row">

      <br />

      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="/admin">Адмінпанель</a></li>
          <li class="active">Управління сторінками</li>
        </ol>
      </div>

      <a href="/admin/pages/create" class="btn btn-default back"><i class="fa fa-plus"></i> Додати сторінку</a>

      <h4>Список сторінок</h4>

      <br />
      <?php if (!empty($pagesList)) : ?>
      <table class="table-bordered table-striped table w-100">
        <tr>
          <th>ID</th>
          <th>Заголовок</th>
          <th>Статус</th>
          <th>Категорія</th>
          <th>Фото</th>
          <th>Дата публікації</th>
          <th>Перегляди</th>
          <th>Посилання на сайт</th>
          <th></th>
          <th></th>
        </tr>
        <?php foreach ($pagesList as $page): ?>
        <tr>
          <td><?= $page['id'] ?></td>
          <td><?= $page['title'] ?></td>
          <td><?= Pages::getStatusText($page['status']) ?></td>
          <td><?php if(isset($page['category'])) echo $page['category'] ?></td>
          <td>
          <?php if (file_exists(ROOT."/uploads/images/page/{$page['id']}.jpg")): ?>
          <img src="<?= "/uploads/images/page/{$page['id']}.jpg" ?>" alt="photo">
          <?php endif; ?>
          </td>
          <td><?= $page['dateposted'] ?></td>
          <td><?= $page['watches'] ?></td>
          <td><a href="/page/view/<?= $page['id'] ?>">Переглянути</a></td>
          <td><a href="/admin/pages/update/<?= $page['id']; ?>" title="Редагувати"><i
                class="fa fa-pencil-square-o"></i></a></td>
          <td><a href="/admin/pages/delete/<?= $page['id']; ?>" title="Видалити"><i class="fa fa-times"></i></a></td>
        </tr>
        <?php endforeach; ?>
      </table>
      <?php else: ?>
      <p>Сторінки відсутні</p>
      <?php endif; ?>

    </div>
  </div>
</section>

<?php require_once(ROOT . '/views/layouts/footer_admin.php'); ?>