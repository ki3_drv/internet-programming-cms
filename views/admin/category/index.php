<?php require_once(ROOT . '/views/layouts/header_admin.php'); ?>

<section>
  <div class="container">
    <div class="row">

      <br />

      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="/admin">Адмінпанель</a></li>
          <li class="active">Управління категоріями</li>
        </ol>
      </div>

      <a href="/admin/category/create" class="btn btn-default back">
        <i class="fa fa-plus"></i> Додати нову категорію
      </a>

      <h4>Категорії</h4>

      <br />

      <?php if (!empty($categoryList)) : ?>
      <table class="table-bordered table-striped table w-100">
        <tr>
          <th>ID</th>
          <th>Назва</th>
          <th>Пріоритетність</th>
          <th></th>
          <th></th>
        </tr>
        <?php foreach ($categoryList as $category): ?>
        <tr>
          <td><?= $category['id'] ?></td>
          <td><?= $category['name'] ?></td>
          <td><?= $category['view_priority'] ?></td>
          <td>
            <a href="/admin/category/update/<?= $category['id']; ?>" title="Редагувати">
              <i class="fa fa-pencil-square-o"></i>
            </a>
          </td>

          <td>
            <a href="/admin/category/delete/<?= $category['id']; ?>" title="Видалити">
              <i class="fa fa-times"></i>
            </a>
          </td>

        </tr>
        <?php endforeach; ?>
      </table>
      <?php else: ?>
      <p>Категорії відсутні</p>
      <?php endif; ?>

    </div>
  </div>
</section>

<?php require_once(ROOT . '/views/layouts/footer_admin.php'); ?>