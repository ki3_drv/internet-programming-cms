 <?php require_once(ROOT.'/views/layouts/header_admin.php'); ?>
 <section>
   <div class="container">
     <div class="row">
       <br>

       <div class="breadcrumbs">
         <ol class="breadcrumb">
           <li><a href="/admin">Адмінпанель</a></li>
           <li><a href="/admin/photos">Управління фотоальбомами</a></li>
           <li class="active">Видалити фотоальбом</li>
         </ol>
       </div>
       <h4>Видалити фотоальбом #<?= $id ?>?</h4>
       <p>Ви дійсно хочете видалити фотоальбом?</p>
       <form method="POST">
         <input type="submit" value="Видалити" name="submit">
       </form>
     </div>
   </div>
 </section>


 <?php require_once(ROOT.'/views/layouts/footer_admin.php'); ?>