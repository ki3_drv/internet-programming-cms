<?php require_once(ROOT . '/views/layouts/header_admin.php'); ?>

<section>
  <div class="container">
    <div class="row">

      <br />

      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="/admin">Адмінпанель</a></li>
          <li><a href="/admin/photos">Управління фотоальбомами</a></li>
          <li class="active"><?= $title ?></li>
        </ol>
      </div>

      <h4><?= $title ?></h4>

      <br />

      <?php if (isset($errors) && is_array($errors)): ?>
      <ul>
        <?php foreach ($errors as $error): ?>
        <li> - <?php echo $error; ?></li>
        <?php endforeach; ?>
      </ul>
      <?php endif; ?>

      <div class="col-lg-6">
        <div class="login-form">
          <div class="login-form">
            <form action="#" method="post" enctype="multipart/form-data">

              <p>Заголовок фотоальбома</p>
              <input type="text" name="title" value="<?= $album['title'] ?>">

              <br />

              <p>Статус</p>
              <select name="status">
                <option value="1" <?php echo ($album['status']==1)? 'selected':''; ?>>Відображається</option>
                <option value="0" <?php echo ($album['status']==0)? 'selected':''; ?>>Не відображається</option>
              </select>

              <br /><br />

              <?php if (!empty($album['photos'])): ?>
              <p>Раніше завантажені фото:</p>
              <div id="upload-images">
                <?php foreach($album['photos'] as $photo): ?>
                <img class="img" src="/uploads/images/albums/<?= $id .'/'. $photo ?>" alt="photo">
                <a href="/admin/photos/delete-image/<?= $id ?>/<?=$photo?>" title="Видалити">
                  <i class="fa fa-times"></i>
                </a><br>
                <?php endforeach; ?>
              </div>
              <? endif; ?>


              <br /><br />
              <div class="preview">

              </div>

              <p>Додати фото:</p>
              <div id="upload-images">
                <input type="file" name="image[]" class="img" onInput="bindEvent();" multiple>
              </div>

              <br /><br />

              <input type="submit" name="submit" class="btn btn-default" value="Зберегти">

              <br /><br />

            </form>
          </div>
        </div>
      </div>
    </div>
</section>

<?php require_once(ROOT . '/views/layouts/footer_admin.php'); ?>