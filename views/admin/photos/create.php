<?php require_once(ROOT . '/views/layouts/header_admin.php'); ?>

<section>
  <div class="container">
    <div class="row">

      <br />

      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="/admin">Адмінпанель</a></li>
          <li><a href="/admin/news">Управління фотоальбомами</a></li>
          <li class="active">Додати фотоальбом</li>
        </ol>
      </div>

      <h4>Додати фотоальбом</h4>

      <br />

      <?php if (isset($errors) && is_array($errors)): ?>
      <ul>
        <?php foreach ($errors as $error): ?>
        <li> - <?php echo $error; ?></li>
        <?php endforeach; ?>
      </ul>
      <?php endif; ?>

      <div class="col-lg-4">
        <div class="login-form">
          <form action="#" method="post" enctype="multipart/form-data">

            <p>Заголовок фотоальбома</p>
            <input type="text" name="title">

            <p>Статус</p>
            <select name="status">
              <option value="1" selected="selected">Відображається</option>
              <option value="0">Не відображається</option>
            </select>

            <br /><br />
            <div class="preview">

            </div>

            <p>Додати фото:</p>
            <div id="upload-images">
              <input type="file" name="image[]" class="img" onInput="bindEvent();" multiple>
            </div>

            <br /><br />

            <input type="submit" name="submit" class="btn btn-default" value="Зберегти" multiple>

            <br /><br />

          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<?php require_once(ROOT . '/views/layouts/footer_admin.php'); ?>