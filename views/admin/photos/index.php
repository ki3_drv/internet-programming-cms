<?php require_once(ROOT . '/views/layouts/header_admin.php'); ?>

<section>
  <div class="container">
    <div class="row">

      <br />

      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="/admin">Адмінпанель</a></li>
          <li class="active">Управління фотоальбомами</li>
        </ol>
      </div>

      <a href="/admin/photos/create" class="btn btn-default back"><i class="fa fa-plus"></i> Додати фотоальбом</a>

      <h4>Список фотоальбомів</h4>

      <br />
      <?php if (!empty($albumsList)): ?>
      <table class="table-bordered table-striped table w-100">
        <tr>
          <th>ID</th>
          <th>Заголовок</th>
          <th>Дата публікації</th>
          <th>Статус</th>
          <th>Фото</th>
          <th>Перегляди</th>
          <th>Посилання на сайт</th>
          <th></th>
          <th></th>
        </tr>
        <?php foreach ($albumsList as $album): ?>
        <tr>
          <td><?= $album['id'] ?></td>
          <td><?= $album['title'] ?></td>
          <td><?= $album['dateposted'] ?></td>
          <td><?= Photos::getStatusText($album['status']) ?></td>

          <td>
            <?php foreach($album['photos'] as $photo): ?>
            <img src="<?= $photo ?>" alt="photo">
            <?php endforeach; ?>
          </td>

          <td><?= $album['watches'] ?></td>
          <td><a href="/photos/view/<?= $album['id'] ?>">Переглянути</a></td>
          <td><a href="/admin/photos/update/<?= $album['id']; ?>" title="Редагувати"><i
                class="fa fa-pencil-square-o"></i></a></td>
          <td><a href="/admin/photos/delete/<?= $album['id']; ?>" title="Видалити"><i class="fa fa-times"></i></a></td>
        </tr>
        <?php endforeach; ?>
      </table>
      <?php else: ?>
      <p>Фотоальбоми відсутні</p>
      <?php endif; ?>

    </div>
  </div>
</section>

<?php require_once(ROOT . '/views/layouts/footer_admin.php'); ?>