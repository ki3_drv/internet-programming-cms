<?php require_once(ROOT . '/views/layouts/header_admin.php'); ?>

<section>
  <div class="container">
    <div class="row">

      <br />

      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="/admin">Адмінпанель</a></li>
          <li><a href="/admin/schedule">Управління розкладом</a></li>
          <li class="active"><?= $title ?></li>
        </ol>
      </div>

      <h4><?= $title ?></h4>

      <br />

      <?php if (isset($errors) && is_array($errors)): ?>
      <ul>
        <?php foreach ($errors as $error): ?>
        <li> - <?php echo $error; ?></li>
        <?php endforeach; ?>
      </ul>
      <?php endif; ?>

      <div class="col-lg-6">
        <div class="login-form">
          <form action="#" method="post" enctype="multipart/form-data">

            <p>Клас</p>
            <input type="text" name="class">


            <p>Статус</p>
            <select name="status">
              <option value="1" selected="selected">Відображається</option>
              <option value="0">Не відображається</option>
            </select>

            <br /><br />

            <?php foreach(AdminScheduleController::$week as $key => $value): ?>
            <p>Розклад на <?= $value ?></p>
            <?php for($i = 1; $i < 9; $i++): ?>
            <?= $i ?>.<input type="text" name="<?= "{$key}_$i" ?>">

            <?php endfor; ?>
            <?php endforeach; ?>




            <br /><br />

            <input type="submit" name="submit" class="btn btn-default" value="Зберегти" multiple>

          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<?php require_once(ROOT . '/views/layouts/footer_admin.php'); ?>