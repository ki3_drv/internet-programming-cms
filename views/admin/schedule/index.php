<?php require_once(ROOT . '/views/layouts/header_admin.php'); ?>

<section>
  <div class="container">
    <div class="row">

      <br />

      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="/admin">Адмінпанель</a></li>
          <li class="active"><?= $title ?></li>
        </ol>
      </div>

      <a href="/admin/schedule/create" class="btn btn-default back">
        <i class="fa fa-plus"></i> Додати розклад
      </a>

      <h4>Розклад</h4>

      <br />

      <?php if (!empty($scheduleList)) : ?>
      <table class="table-bordered table-striped table w-100">
        <tr>
          <th>ID</th>
          <th>Клас</th>
          <th>Понеділок</th>
          <th>Вівторок</th>
          <th>Середа</th>
          <th>Четвер</th>
          <th>П'ятниця</th>
          <th>Статус</th>
          <th></th>
          <th></th>

        </tr>
        <?php foreach ($scheduleList as $schedule): ?>
        <tr>
          <td><?= $schedule['id'] ?></td>
          <td><?= $schedule['class'] ?></td>


          <?php foreach($schedule['schedule'] as $week => $one_day_schedule): ?>

          <td>
            <?php for($i = 1; $i < 9; $i++): ?>
            <?php if($one_day_schedule->$i !== ""): ?>
            <p><?= "{$i}.".$one_day_schedule->$i ?></p>
            <?php endif; ?>
            <?php endfor; ?>
          </td>
          <?php endforeach; ?>


          <td><?= Schedule::getStatusText($schedule['status']) ?></td>

          <td>
            <a href="/admin/schedule/update/<?= $schedule['id']; ?>" title="Редагувати">
              <i class="fa fa-pencil-square-o"></i>
            </a>
          </td>

          <td>
            <a href="/admin/schedule/delete/<?= $schedule['id']; ?>" title="Видалити">
              <i class="fa fa-times"></i>
            </a>
          </td>

        </tr>
        <?php endforeach; ?>

      </table>
      <?php else: ?>
      <p>Розклад відсутній</p>
      <?php endif; ?>

    </div>
  </div>
</section>

<?php require_once(ROOT . '/views/layouts/footer_admin.php'); ?>