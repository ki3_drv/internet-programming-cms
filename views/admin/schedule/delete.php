 <?php require_once(ROOT.'/views/layouts/header_admin.php'); ?>
 <section>
   <div class="container">
     <div class="row">
       <br>

       <div class="breadcrumbs">
         <ol class="breadcrumb">
           <li><a href="/admin">Адмінпанель</a></li>
           <li><a href="/admin/schedule">Управління розкладом</a></li>
           <li class="active"><?= $title ?></li>
         </ol>
       </div>
       <h4><?= $title ?></h4>
       <p>Ви дійсно хочете видалити розклад для <?= $schedule['class'] ?> класу?</p>
       <form method="GET">
         <input type="submit" value="Видалити" name="submit">
       </form>
     </div>
   </div>
 </section>


 <?php require_once(ROOT.'/views/layouts/footer_admin.php'); ?>