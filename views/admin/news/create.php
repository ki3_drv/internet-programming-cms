<?php require_once(ROOT . '/views/layouts/header_admin.php'); ?>

<section>
  <div class="container">
    <div class="row">

      <br />

      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="/admin">Адмінпанель</a></li>
          <li><a href="/admin/news">Управління новинами</a></li>
          <li class="active">Додати новину</li>
        </ol>
      </div>

      <h4>Додати новину</h4>

      <br />

      <?php if (isset($errors) && is_array($errors)): ?>
      <ul>
        <?php foreach ($errors as $error): ?>
        <li> - <?php echo $error; ?></li>
        <?php endforeach; ?>
      </ul>
      <?php endif; ?>

      <div class="col-lg-4">
        <div class="login-form">
          <form action="#" method="post" enctype="multipart/form-data">

            <p>Заголовок новини</p>
            <textarea name="title" rows="1"></textarea>

            <p>Детальний опис</p>
            <textarea name="description"></textarea>

            <p>Зміст новини</p>
            <textarea name="content" rows="20"></textarea>

            <p>Фотоальбом</p>
            <select name="album_id">

              <?php if (is_array($albumsList)): ?>
              <option value="0">----- Прикріпити фотоальбом -----</option>
              <?php foreach ($albumsList as $album): ?>
              <option value="<?= $album['id'] ?>">
                <?= $album['title'].' ['.$album['dateposted'].']' ?>
              </option>
              <?php endforeach; ?>
              <?php endif; ?>
            </select>

            <br /><br />

            <div class="preview">

            </div>
            <p>Зображення для новини</p>
            <input type="file" name="image" class="img" onInput="bindEvent();">


            <br /><br />

            <p>Статус</p>
            <select name="status">
              <option value="1" selected="selected">Відображається</option>
              <option value="0">Не відображається</option>
            </select>

            <br /><br />

            <input type="submit" name="submit" class="btn btn-default" value="Зберегти">

            <br /><br />

          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<?php require_once(ROOT . '/views/layouts/footer_admin.php'); ?>