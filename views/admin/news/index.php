<?php require_once(ROOT . '/views/layouts/header_admin.php'); ?>

<section>
  <div class="container">
    <div class="row">

      <br />

      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="/admin">Адмінпанель</a></li>
          <li class="active">Управління новинами</li>
        </ol>
      </div>

      <a href="/admin/news/create" class="btn btn-default back"><i class="fa fa-plus"></i> Додати новину</a>

      <h4>Список Новин</h4>

      <br />

      <?php if (!empty($newsList)): ?>
      <table class="table-bordered table-striped table w-100">
        <tr>
          <th>ID</th>
          <th>Заголовок</th>
          <th>Опис</th>
          <th>Статус</th>
          <th>Назва альбома</th>
          <th>Дата публікації</th>
          <th>Картинка</th>
          <th>Перегляди</th>
          <th>Посилання на сайт</th>
          <th></th>
          <th></th>
        </tr>
        <?php foreach ($newsList as $news): ?>
        <tr>
          <td><?= $news['id'] ?></td>
          <td><?= $news['title'] ?></td>
          <td><?= $news['description'] ?></td>
          <td><?= News::getStatusText($news['status']) ?></td>
          <td><?= $news['album_title'] ?></td>
          <td><?= $news['dateposted'] ?></td>
          <td>
            <?php if(file_exists('./uploads/images/news/'.$news['id'].'.jpg')): ?>
            <img src="/uploads/images/news/<?= $news['id'] ?>.jpg">
            <?php endif; ?>
          </td>
          <td><?= $news['watches'] ?></td>
          <td><a href="/news/view/<?= $news['id'] ?>">Переглянути</a></td>
          <td><a href="/admin/news/update/<?= $news['id']; ?>" title="Редагувати"><i
                class="fa fa-pencil-square-o"></i></a></td>
          <td><a href="/admin/news/delete/<?= $news['id']; ?>" title="Видалити"><i class="fa fa-times"></i></a></td>
        </tr>
        <?php endforeach; ?>
      </table>
      <?php else: ?>
      <p>Новини відсутні</p>
      <?php endif; ?>

    </div>
  </div>
</section>

<?php require_once(ROOT . '/views/layouts/footer_admin.php'); ?>