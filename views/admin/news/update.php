<?php require_once(ROOT . '/views/layouts/header_admin.php'); ?>

<section>
  <div class="container">
    <div class="row">

      <br />

      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="/admin">Адмінпанель</a></li>
          <li><a href="/admin/news">Управління новинами</a></li>
          <li class="active"><?= $title ?></li>
        </ol>
      </div>

      <h4><?= $title ?></h4>

      <br />

      <?php if (isset($errors) && is_array($errors)): ?>
      <ul>
        <?php foreach ($errors as $error): ?>
        <li> - <?php echo $error; ?></li>
        <?php endforeach; ?>
      </ul>
      <?php endif; ?>

      <div class="col-lg-6">
        <div class="login-form">
          <form action="#" method="post" enctype="multipart/form-data">

            <p>Заголовок новини</p>
            <input type="text" name="title" value="<?= $news['title'] ?>">

            <p>Детальний опис</p>
            <textarea name="description"><?= $news['description'] ?></textarea>

            <p>Зміст новини</p>
            <textarea name="content" rows="20"><?= $news['content'] ?></textarea>

            <p>Фотоальбом</p>
            <select name="album_id">

              <?php if (is_array($albumsList)): ?>
              <option value="0">----- Без фотоальбома -----</option>
              <?php foreach ($albumsList as $album): ?>

              <option value="<?= $album['id'] ?>" <?php if($album['id'] == $news['album_id']) echo 'selected' ?>>
                <?= '#'.$album['id'].' '.$album['title'].' ['.$album['dateposted'].']' ?>
              </option>
              <?php endforeach; ?>
              <?php endif; ?>
            </select>

            <br /><br />
            <?php if (file_exists("./uploads/images/news/{$id}.jpg")): ?>
            <div id="upload-images">
              <img class="img" src="/uploads/images/news/<?= $id ?>.jpg" alt="news_image">
              <a href="/admin/news/delete-image/<?= $id ?>" title="Видалити">
                <i class="fa fa-times"></i>
              </a>
            </div>      
            <?php endif; ?>
            <br />
            <div class="preview"></div>
            <p>Нове зображення новини</p>
            <input type="file" name="image" class="img" onInput="bindEvent();">


            <br /><br />

            <p>Статус</p>
            <select name="status">
              <option value="1" <?php echo ($news['status']==1)? 'selected':''; ?>>Відображається</option>
              <option value="0" <?php echo ($news['status']==0)? 'selected':''; ?>>Не відображається</option>
            </select>

            <br /><br />

            <input type="submit" name="submit" class="btn btn-default" value="Сохранить">

            <br /><br />

          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<?php require_once(ROOT . '/views/layouts/footer_admin.php'); ?>