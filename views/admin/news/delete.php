 <?php require_once(ROOT.'/views/layouts/header_admin.php'); ?>
 <section>
   <div class="container">
     <div class="row">
       <br>

       <div class="breadcrumbs">
         <ol class="breadcrumb">
           <li><a href="/admin">Адмінпанель</a></li>
           <li><a href="/admin/news">Управління новинами</a></li>
           <li class="active">Видалити новину</li>
         </ol>
       </div>
       <h4>Видалити новину #<?= $id ?>?</h4>
       <p>Ви дійсно хочете видалити новину?</p>
       <form method="POST">
         <input type="submit" value="Видалити" name="submit">
       </form>
     </div>
   </div>
 </section>


 <?php require_once(ROOT.'/views/layouts/footer_admin.php'); ?>