<?php require_once(ROOT . '/views/layouts/header_admin.php'); ?>

<section>
  <div class="container">
    <?php if (isset($_SESSION['admin'])): ?>
    <div class="row">
      <br />
      <h4>Доброго дня, адміністратор!</h4>
      <br />
      <p>Вам доступні наступні можливості:</p>
      <br />
      <ul>
        <li><a href="/admin/news">Управління новинами</a></li>
        <li><a href="/admin/photos">Управління фотогалереєю</a></li>
        <li><a href="/admin/pages">Управління сторінками</a></li>
        <li><a href="/admin/category">Управління категоріями</a></li>
        <li><a href="/admin/links">Управління посиланнями</a></li>
        <li><a href="/admin/schedule">Управління розкладом уроків</a></li>
        <li><a href="/admin/collective">Управління інформацією про колектив</a></li>
      </ul>
      <?php else: ?>
      <h1 class="text-center">Вхід</h1>

      <?php if(isset($errors)): ?>
        <div id="error-list" class="">
          <?php foreach($errors as $error): ?>
            <p class="login-error text-center">&dot; <?= $error ?></p>
          <?php endforeach; ?>
        </div>
      <?php endif; ?>

      <div id="login-form" class="">
        <form action="/admin/login/" class="text-center" method="POST">
          <label for="login">Введіть логін:<br>
            <input type="text" name="login">
          </label><br><br>
          <label for="password">Введіть пароль:<br>
            <input type="password" name="password">
          </label><br>
          <div class="captcha">Captcha will be here!</div>
          <input class="login" type="submit" value="Війти" name="submit">
        </form>
      </div>
      <?php endif; ?>

    </div>
  </div>
</section>

<?php require_once(ROOT . '/views/layouts/footer_admin.php'); ?>