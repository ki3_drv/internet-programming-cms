<?php require_once(ROOT . '/views/layouts/header_admin.php'); ?>

<section>
  <div class="container">
    <div class="row">

      <br />

      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="/admin">Адмінпанель</a></li>
          <li><a href="/admin/collective">Управління персоналом</a></li>
          <li class="active">Додати нову інформацію</li>
        </ol>
      </div>

      <h4>Додати нову інформацію</h4>

      <br />

      <?php if (isset($errors) && is_array($errors)): ?>
      <ul>
        <?php foreach ($errors as $error): ?>
        <li> - <?php echo $error; ?></li>
        <?php endforeach; ?>
      </ul>
      <?php endif; ?>

      <div class="col-lg-6">
        <div class="login-form">
          <form action="#" method="post" enctype="multipart/form-data">

            <p>Прізвище</p>
            <input type="text" name="l_name">

            <p>Ім'я</p>
            <input type="text" name="f_name">

            <p>По-батькові</p>
            <input type="text" name="m_name">

            <p>Посада</p>
            <input type="text" name="post">

            <br />
            <label for="teacher">
              <input id="teacher" type="checkbox" name="is_teacher" onClick="toggleTeachrInfo();">Є вчителем
            </label>

            <div class="teacher-info hidden">
              <br />

              <label for="admin">
                <input id="admin" type="checkbox" name="is_admin">Входить до адміністрації
              </label>

              <br />

              <p>Освіта</p>
              <input type="text" name="education" id="education">

              <p>Спеціалізація</p>
              <input type="text" name="speciality" id="speciality">

              <p>Категорія</p>
              <input type="text" name="cathegory" id="cathegory">
            </div>

            <br />

            <p>Коротка біографія(додаткова інформація)</p>
            <textarea name="biography" rows="10"></textarea>

            <br /><br />
            <div class="preview"></div>
            <p>Фото</p>
            <input type="file" name="photo" class="img" onInput="bindEvent();">

            <br />

            <input type="submit" name="submit" class="btn btn-default" value="Зберегти">

            <br /><br />

          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<?php require_once(ROOT . '/views/layouts/footer_admin.php'); ?>