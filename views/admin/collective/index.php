<?php require_once(ROOT . '/views/layouts/header_admin.php'); ?>

<section>
  <div class="container">
    <div class="row">

      <br />

      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="/admin">Адмінпанель</a></li>
          <li class="active">Управління персоналом</li>
        </ol>
      </div>

      <a href="/admin/collective/create" class="btn btn-default back"><i class="fa fa-plus"></i> Додати нову
        інформацію</a>

      <h4>Персонал школи</h4>

      <br />

      <table class="table-bordered table-striped table w-100">
        <tr>
          <th>ID</th>
          <th>ПІБ</th>
          <th>Вид обов'язків</th>
          <th>Посада</th>
          <th>Освіта</th>
          <th>Спеціалізація</th>
          <th>Категорія</th>
          <th>Фото</th>
          <th></th>
          <th></th>
        </tr>
        <?php foreach ($peopleList as $person): ?>
        <tr>
          <td><?= $person['id'] ?></td>
          <td><?= $person['l_name'].' '.$person['f_name'].' '.$person['m_name'] ?></td>
          <td>
            <?php echo ($person['is_teacher'])? 'Вчитель' : 'Представник обслуговуючого персоналу' ; ?>
            <?php echo ($person['is_admin'])? ', входить до адміністрації школи' : '' ; ?>
          </td>
          <td><?= $person['post'] ?></td>
          <?php if($person['is_teacher']): ?>
          <td><?= $person['education'] ?></td>
          <td><?= $person['speciality'] ?></td>
          <td><?= $person['cathegory'] ?></td>
          <?php else: ?>
          <td></td>
          <td></td>
          <td></td>
          <?php endif; ?>
          <td><img src="/uploads/images/collective/<?= $person['id'] ?>.jpg"></td>
          <td><a href="/admin/collective/update/<?= $person['id']; ?>" title="Редагувати"><i
                class="fa fa-pencil-square-o"></i></a></td>
          <td><a href="/admin/collective/delete/<?= $person['id']; ?>" title="Видалити"><i class="fa fa-times"></i></a>
          </td>
        </tr>
        <?php endforeach; ?>
      </table>

    </div>
  </div>
</section>

<?php require_once(ROOT . '/views/layouts/footer_admin.php'); ?>