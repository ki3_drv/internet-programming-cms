<?php require_once(ROOT . '/views/layouts/header_admin.php'); ?>

<section>
  <div class="container">
    <div class="row">

      <br />

      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="/admin">Адмінпанель</a></li>
          <li><a href="/admin/collective">Управління персоналом</a></li>
          <li class="active"><?= $title ?></li>
        </ol>
      </div>

      <h4><?= $title ?></h4>

      <br />

      <?php if (isset($errors) && is_array($errors)): ?>
      <ul>
        <?php foreach ($errors as $error): ?>
        <li> - <?php echo $error; ?></li>
        <?php endforeach; ?>
      </ul>
      <?php endif; ?>

      <div class="col-lg-6">
        <div class="login-form">
          <form action="#" method="post" enctype="multipart/form-data">

            <p>Прізвище</p>
            <input type="text" name="l_name" value="<?= $person['l_name'] ?>">

            <p>Ім'я</p>
            <input type="text" name="f_name" value="<?= $person['f_name'] ?>">

            <p>По-батькові</p>
            <input type="text" name="m_name" value="<?= $person['m_name'] ?>">

            <p>Посада</p>
            <input type="text" name="post" value="<?= $person['post'] ?>">

            <div class="teacher-info <?php if (!$person['is_teacher']) echo 'hidden' ?>">
              <br />

              <label for="admin">
                <input id="admin" type="checkbox" name="is_admin">Входить до адміністрації
              </label>

              <br />

              <p>Освіта</p>
              <input type="text" name="education" id="education" 
                                value="<?php if(!empty($teacher)) echo $teacher['education']; ?>">

              <p>Спеціалізація</p>
              <input type="text" name="speciality" id="speciality" 
                                value="<?php if(!empty($teacher)) echo $teacher['speciality']; ?>">

              <p>Категорія</p>
              <input type="text" name="cathegory" id="cathegory" 
                                value="<?php if(!empty($teacher)) echo $teacher['cathegory']; ?>">
            </div>

            <br />

            <p>Коротка біографія(додаткова інформація)</p>
            <textarea name="biography" rows="10"><?= $person['biography'] ?>></textarea>

            <br /><br />

            <?php if (file_exists("./uploads/images/collective/{$id}.jpg")): ?>
            <div id="upload-images">
              <img class="img" src="/uploads/images/collective/<?= $id ?>.jpg" alt="photo">
              <a href="/admin/collective/delete-image/<?= $id ?>" title="Видалити">
                <i class="fa fa-times"></i>
              </a>
            </div>
            <?php endif; ?>
            <br />
            <div class="preview"></div>
            <p>Нове фото</p>
            <input type="file" name="photo" class="img" onInput="bindEvent();">

            <br />

            <input type="submit" name="submit" class="btn btn-default" value="Зберегти">

            <br /><br />

          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<?php require_once(ROOT . '/views/layouts/footer_admin.php'); ?>