<footer>
  <div>
    <p>&copy;2019. Всі права захищені</p>
    <p>Якубівська ЗОШ 1-2 ступенів</p>
  </div>
  <?php $links = Links::getLinksList(); ?>
  <div id="links">
    <?php foreach($links as $link): ?>
    <a href="<?= $link['url'] ?>"><?= $link['title'] ?></a>
    <?php endforeach; ?>
  </div>
</footer>
</body>

</html>