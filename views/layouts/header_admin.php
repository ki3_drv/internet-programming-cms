<!DOCTYPE html>
<html lang="uk">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?= $title ?></title>
  <link href="/public/css/bootstrap.min.css" rel="stylesheet">
  <link href="/public/css/font-awesome.min.css" rel="stylesheet">
  <link href="/public/css/main.css" rel="stylesheet">
  <link href="/public/css/responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="/public/css/login.css">

</head>
<!--/head-->
<body>
  <div class="page-wrapper">

    <header id="header">
      <!--header-->
      <div class="header_top">
        <!--header_top-->
        <div class="container">
          <div class="row">
            <div class="col-sm-6">
              <div class="contactinfo">
                <h5>
                  <a href="/admin"><i class="fa fa-edit"></i> Адмінпанель</a>
                </h5>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="social-icons pull-right">
                <ul class="nav navbar-nav">
                  <li><a href="/"><i class="fa fa-sign-out"></i>На сайт</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--/header_top-->