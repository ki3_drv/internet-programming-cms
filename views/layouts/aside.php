<aside>
  <section id="director">
    <h3>Директор</h3>
    <div class="content">
      <div class="photos">
        <img src="/public/img/foto.jpg">
      </div>
      <p>Марчук Олександр Дмитрович</p>
    </div>
  </section>

  <?php 
  $pagesArr = array();
  $categoryList = Category::getCategoryList();

  foreach($categoryList as $category) {
    $pages = Pages::getPagesByCategoryId($category['id']);
    if (!empty($pages)) {
      $pagesArr[$category['name']] = $pages; 
    }
  }
?>
  <?php foreach($pagesArr as $category => $pages): ?>
  <?php if(!empty($pages)): ?>
  <section>
    <h3><?= $category ?></h3>
    <div class="content">
      <?php foreach($pages as $page): ?>
      <p><a href="/page/view/<?= $page['id'] ?>" class="link">
          <?= $page['title'] ?>
        </a></p>
      <?php endforeach; ?>
    </div>
  </section>
  <?php endif; ?>
  <?php endforeach; ?>

  <!--<section id="announce">
    <h3>Анонси подій</h3>
    <div class="content">
      <p><a href="" class="link">Анонс №1</a></p>
      <p><a href="" class="link">Анонс №2</a></p>
      <p><a href="" class="link">Анонс №3</a></p>
      <p><a href="" class="link">Анонс №4</a></p>
    </div>
  </section>
  <section>
    <h3>Виконання ст.30 закону України "про освіту"</h3>
    <div class="content">
      <p><a href="" class="link">Анонс №1</a></p>
      <p><a href="" class="link">Анонс №2</a></p>
      <p><a href="" class="link">Анонс №3</a></p>
      <p><a href="" class="link">Анонс №4</a></p>
    </div>
  </section>
  <section>
    <h3>Поточна діяльність</h3>
    <div class="content">
      <p><a href="" class="link">Анонс №1</a></p>
      <p><a href="" class="link">Анонс №2</a></p>
      <p><a href="" class="link">Анонс №3</a></p>
      <p><a href="" class="link">Анонс №4</a></p>
    </div>
  </section>
  <section>
    <h3>Нова українська школа</h3>
    <div class="content">
      <p><a href="" class="link">Анонс №1</a></p>
      <p><a href="" class="link">Анонс №2</a></p>
      <p><a href="" class="link">Анонс №3</a></p>
      <p><a href="" class="link">Анонс №4</a></p>
    </div>
  </section>
  <section>
    <h3>Інформація для учнів</h3>
    <div class="content">
      <p><a href="" class="link">Анонс №1</a></p>
      <p><a href="" class="link">Анонс №2</a></p>
      <p><a href="" class="link">Анонс №3</a></p>
      <p><a href="" class="link">Анонс №4</a></p>
    </div>
  </section>
  <section>
    <h3>Інформація для батьків</h3>
    <div class="content">
      <p><a href="" class="link">Рекомендації батькам учнів середніх класів щодо підготовки домашніх завдань</a></p>
      <p><a href="" class="link">Анонс №1</a></p>
      <p><a href="" class="link">Анонс №2</a></p>
      <p><a href="" class="link">Анонс №3</a></p>
      <p><a href="" class="link">Анонс №4</a></p>
    </div>
  </section>
  <section>
    <h3>Посилання</h3>
    <div class="content">
      <p><a href="" class="link">Анонс №1</a></p>
      <p><a href="" class="link">Анонс №2</a></p>
      <p><a href="" class="link">Анонс №3</a></p>
      <p><a href="" class="link">Анонс №4</a></p>
    </div>
  </section>-->
</aside>