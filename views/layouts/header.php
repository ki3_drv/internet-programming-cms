<!DOCTYPE html>
<html lang="uk">

<head>
  <meta charset="UTF-8">
  <title> <?= $title ?></title>

  <link href="/public/css/font-awesome.min.css" rel="stylesheet">
  <link href="/public/css/load-animation.css" rel="stylesheet">
  <link rel="stylesheet" href="/public/css/style.css">
  
</head>

<body>
  <header>

    <nav>
        <ul class=" topmenu">
        <li><a href="/">Головна</a></li>
        <li><a href="/page/view/2">Візитка</a></li>
        <li><a href="/page/view/3">Історія</a></li>
        <li><a href="" class="submenu-link">Режим роботи</a>
          <ul class="submenu">
            <li><a href="/schedule/bells/">Розклад дзвінків</a></li>
            <li><a href="/schedule/classes/">Розклад уроків</a></li>
            <li><a href="/schedule/admission/">Графік прийому громадян</a></li>
          </ul>
        </li>
        <li><a href="" class="submenu-link">Колектив</a>
          <ul class="submenu">
            <li><a href="/collective/admins/">Адміністрація</a></li>
            <li><a href="/collective/teachers/">Вчителі</a></li>
            <li><a href="/collective/service/">Обслуговуючий персонал</a></li>
          </ul>
        </li>
        <li><a href="/news/">Новини</a></li>
        <li><a href="/photos/">Фото</a></li>
        <li><a href="/page/view/4">Контакти</a></li>
        </ul>
        </nav>
  </header>