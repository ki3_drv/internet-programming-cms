<?php require_once(ROOT . '/views/layouts/header.php'); ?>

<main>
  <div id="content">
    <section>
      <h3><?= $news['title'] ?></h3>
      <?php if (file_exists(ROOT.'/uploads/images/news/'.$id.'.jpg')): ?>
      <div class="image">
        <img src="<?= '/uploads/images/news/'.$id.'.jpg' ?>" alt="Image"><br>
      </div>
      <?php endif; ?>

      <div class="content">
        <?= $news['content'] ?>
      </div>
      <?php if(!empty($images)): ?>
      <div class="image">
        <?php foreach($images as $image): ?>
        <img src="/uploads/images/albums/<?= $news['album_id'].'/'.$image ?>" alt="">
        <?php endforeach; ?>
      </div>
      <?php endif; ?>

      <hr>
      <div class="date-view-info">
        <span>Переглядів: <?= $news['watches'] ?></span>
        <span>Дата публікації: <?= $news['dateposted'] ?></span>
      </div>


    </section>
  </div>

  <?php require_once(ROOT . '/views/layouts/aside.php'); ?>

</main>

<?php require_once(ROOT . '/views/layouts/footer.php'); ?>