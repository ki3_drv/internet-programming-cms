<?php foreach($news as $item): ?>
<div class="news_item">
  <?php if(file_exists('./uploads/images/news/'.$item['id'].'.jpg')): ?>
  <img src="<?= '/uploads/images/news/'.$item['id'].'.jpg' ?>" alt="news">
  <?php endif ?>
  <h4><?= $item['title'] ?></h4>
  <p><?= $item['description'] ?></p>
  <span><?= $item['dateposted'] ?></span>
  <a href="/news/view/<?= $item['id'] ?>">Докладніше</a>

</div>
<hr>
<?php endforeach; ?>