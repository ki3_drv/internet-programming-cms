<?php require_once(ROOT . '/views/layouts/header.php'); ?>

<link rel="stylesheet" href="/public/css/accordion.css">
<script type="text/javascript" src="/public/js/modernizr.js"></script>
<main class="no-js">
<script>
  document.querySelector('.no-js').classList.replace('no-js', 'js');
</script>
  <div id="content">
    <section>
      <h3>Новини</h3>
      <div class="content">



        <ul class="cd-accordion-menu animated">
          <?php foreach($news as $year => $months): ?>
          <li class="has-children" style="display: block">
            <input type="checkbox" name="group-<?= $year ?>" id="group-<?= $year ?>" checked>
            <label for="group-<?= $year ?>">Новини за <?= $year ?> рік</label>

            <ul>
              <?php foreach($months as $month => $items): ?>
              <li class="has-children" style="display: block">
                <input type="checkbox" name="sub-group-<?= $year.$month ?>" id="sub-group-<?= $year.$month ?>">
                <label for="sub-group-<?= $year.$month ?>">Новини за <?= $month ?> <?= $year ?> року</label>

                <ul>
                  <?php foreach($items as $news_item): ?>
                  <li><a href="/news/view/<?= $news_item['id']; ?>"><?= $news_item['title']; ?></a></li>
                  <?php endforeach; ?>
                </ul>

              </li>
              <?php endforeach; ?>



            </ul>
          </li>

          <?php endforeach; ?>
        </ul>

      </div>
    </section>
  </div>
  <?php require_once(ROOT . '/views/layouts/aside.php'); ?>

</main>

<?php require_once(ROOT . '/views/layouts/footer.php'); ?>

<script type="text/javascript" src="/public/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/public/js/accordion.js"></script> <!-- Resource jQuery -->