<?php require_once(ROOT . '/views/layouts/header.php'); ?>
<main>
  <div id="content">
    <section>
      <h3>Класи</h3>
      <div class="content">
        <table>

          <tr>
            <th></th>
            <th>Понеділок</th>
            <th>Вівторок</th>
            <th>середа</th>
            <th>Четвер</th>
            <th>П'ятниця</th>
          </tr>

          <?php for($i = 1, $time = 0; $i < 9; $i++, $time++): ?>

          <tr class="content">
            <th><?= $times[$time] ?></th>
            <?php foreach($schedule['schedule'] as $week => $one_day_schedule): ?>
            <?php //var_dump($schedule); die("yep");?>

            <?php if($one_day_schedule->$i !== ""): ?>
            <td class="content">
              <div class="subject"><?= $one_day_schedule->$i ?></div>
            </td>
            <?php else: ?>
            <td></td>
            <?php endif; ?>

            <?php endforeach; ?>

          </tr>

          <?php endfor; ?>


        </table>



      </div>
    </section>
  </div>
  <?php require_once(ROOT . '/views/layouts/aside.php'); ?>

</main>
<?php require_once(ROOT . '/views/layouts/footer.php'); ?>