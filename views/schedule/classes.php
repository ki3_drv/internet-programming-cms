<?php require_once(ROOT.'/views/layouts/header.php'); ?>

<link rel="stylesheet" href="/public/css/accordion.css">
<main>
  <div id="content">
    <section>
      <h3><?= $title ?></h3>
      <div class="content">

        <ul class="cd-accordion-menu animated">
          <?php foreach ($scheduleList as $schedule): ?>
          <li>
            <a href="/schedule/classes/<?= $schedule['id'] ?>" class="class">
              <?= $schedule['class'] ?> клас
            </a>
          </li>
          <?php endforeach; ?>

        </ul>
      </div>
    </section>
  </div>
  <?php require_once(ROOT . '/views/layouts/aside.php'); ?>

</main>
<?php require_once(ROOT . '/views/layouts/footer.php'); ?>