<?php require_once(ROOT . '/views/layouts/header.php'); ?>
<main>
  <div id="content">
    <section>
      <h3>Розклад дзвінків</h3>
      <div class="content">
        <table class="schedule">
          <tr>
            <th>№ урока</th>
            <th>Початок</th>
            <th>Закінчення</th>
          </tr>
          <tr>
            <th>1</th>
            <td class="content">9:00</td>
            <td class="content">9:45</td>
          </tr>
          <tr>
            <th>2</th>
            <td class="content">9:55</td>
            <td class="content">10:40</td>
          </tr>
          <tr>
            <th>3</th>
            <td class="content">11:00</td>
            <td class="content">11:45</td>
          </tr>
          <tr>
            <th>4</th>
            <td class="content">12:05</td>
            <td class="content">12:50</td>
          </tr>
          <tr>
            <th>5</th>
            <td class="content">13:00</td>
            <td class="content">13:45</td>
          </tr>
          <tr>
            <th>6</th>
            <td class="content">13:55</td>
            <td class="content">14:40</td>
          </tr>
          <tr>
            <th>7</th>
            <td class="content">14:50</td>
            <td class="content">15:35</td>
          </tr>
          <tr>
            <th>8</th>
            <td class="content">15:45</td>
            <td class="content">16:30</td>
          </tr>
        </table>
      </div>
      </section>
  </div>
  <?php require_once(ROOT . '/views/layouts/aside.php'); ?>

</main>
<?php require_once(ROOT . '/views/layouts/footer.php'); ?>
