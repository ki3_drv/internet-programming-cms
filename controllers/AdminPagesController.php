<?php

/**
 * Контроллер AdminNewsController
 * Управление новостями в админпанели
 */
class AdminPagesController extends AdminBase
{

    /**
     * Action для страницы "Управление новостями"
     */
    public function actionIndex()
    {
        // Проверка доступа
        self::checkAdmin();

        // Получаем список новостей
        $pagesList = Pages::getPagesListAdmin();

        for($i = 0; $i < count($pagesList); $i++) {
          $pagesList[$i]['category'] = 
                Category::getCategoryNameById($pagesList[$i]['category_id']);
        }
        
        $title = 'Управління сторінками';

        // Подключаем вид
        require_once(ROOT . '/views/admin/pages/index.php');
        return true;
    }

    /**
     * Action для страницы "Добавить новость"
     */
    public function actionCreate()
    {
        // Проверка доступа
        self::checkAdmin();

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена
            // Получаем данные из формы
            $options = array();
            $options['title'] = $_POST['title'];
            $options['content'] = $_POST['content'];
            $options['category_id'] = $_POST['category_id'];
            $options['status'] = $_POST['status'];

            // Флаг ошибок в форме
            $errors = false;

            // При необходимости можно валидировать значения нужным образом
            if (!isset($options['title']) || empty($options['title']) || 
                !isset($options['content']) || empty($options['content'])) {
                $errors[] = 'Заповніть обов\'язкові поля поля';
            }

            if ($errors == false) {
                // Если ошибок нет
                // Добавляем новую новость
                $options['dateposted'] = date('Y-m-d H:i:s', time());
                $id = Pages::createPage($options);

                if (isset($id) && is_uploaded_file($_FILES['image']['tmp_name'])) {
                    move_uploaded_file($_FILES['image']['tmp_name'], 
                                        ROOT.'/uploads/images/page/'.$id.'.jpg');
                };

                // Перенаправляем пользователя на страницу управлениями новостями
                header("Location: /admin/pages");
            }
        }

        $title = 'Додати сторінку';
        $categoryList = Category::getCategoryList();

        require_once(ROOT . '/views/admin/pages/create.php');
        return true;
    }

    /**
     * Action для страницы "Редактировать новость"
     */
    public function actionUpdate($id)
    {
        // Проверка доступа
        self::checkAdmin();

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена   
            // Получаем данные из формы
            $options = array();
            $options['title'] = $_POST['title'];
            $options['content'] = $_POST['content'];
            $options['category_id'] = $_POST['category_id'];
            $options['status'] = $_POST['status'];

            // Флаг ошибок в форме
            $errors = false;

            // При необходимости можно валидировать значения нужным образом
            if (!isset($options['title']) || empty($options['title']) || 
                !isset($options['content']) || empty($options['content'])) {
                $errors[] = 'Заповніть обов\'язкові поля поля';
            }

            if ($errors == false) {
                // Если ошибок нет
                // Редактируем новость
                Pages::updatePageById($id, $options);

                if (is_uploaded_file($_FILES['image']['tmp_name'])) {
                    move_uploaded_file($_FILES['image']['tmp_name'], 
                                        ROOT.'/uploads/images/page/'.$id.'.jpg');
                };


                // Перенаправляем пользователя на страницу управлениями новостями
                header("Location: /admin/pages");
            }
        }
        // Получаем данные о конкретной новости
        $page = Pages::getPageById($id);
        $categoryList = Category::getCategoryList();
        $title = 'Редагувати сторінку';
        // Подключаем вид
        require_once(ROOT . '/views/admin/pages/update.php');
        return true;
    }


    /**
     * Action для страницы "Удалить новость"
     */
    public function actionDelete($id)
    {
        // Проверка доступа
        self::checkAdmin();

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена
            // Удаляем новость
            Pages::deletePageById($id);

            if(file_exists(ROOT."/uploads/images/page/{$id}.jpg")) {
                unlink(ROOT."/uploads/images/page/{$id}.jpg");
            }
             
            // Перенаправляем пользователя на страницу управлениями новостями
            header("Location: /admin/pages");
        }
        $title = 'Видалити сторінку';
        // Подключаем вид
        require_once(ROOT . '/views/admin/pages/delete.php');
        return true;
    }

    public function actionDeleteImage($id) {
        if (file_exists("./uploads/images/page/{$id}.jpg")) {
            unlink("./uploads/images/page/{$id}.jpg");
        }

        header("Location: /admin/pages/update/{$id}");
    }
}
