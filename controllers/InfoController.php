<?php

class FilmsController
{
  public function actionAll() {
    $filmListAll = Films::getFilmsAll();
    require_once(ROOT.'/views/Films/viewAll.php');
  }

  public function actionNewest() {
    $filmList = Films::getFilmListNew();
  }

  public function actionView($id) {
    echo 'Controller'.$id;
    $filmObj = Films::getFilmItem($id);
    $bestFilms = Films::getBestFilms(4);
    require_once(ROOT.'/views/Films/view.php');
  }

}
