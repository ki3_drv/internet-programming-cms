<?php

/**
 * Контроллер AdminController
 * Главная страница в админпанели
 */
class AdminController extends AdminBase
{
    /**
     * Action для стартовой страницы "Панель администратора"
     */
    public function actionIndex() {
        $title = 'Адмінпанель';
        require_once(ROOT . '/views/admin/index.php');
        return true;
    }

    public function actionLogin() {
        $_SESSION['admin'] = null;
        $title = 'Вхід';
        if (isset($_POST['submit'])) {
            $errors = array();

            if (Users::userExists($_POST['login'])) {

                if (Users::passwordIsCorrect($_POST['login'], $_POST['password']))
                    $_SESSION['admin'] = true;
                else
                    $errors[] = 'Невірний пароль';

            } else {
                $errors[] = 'Невірний логін';
            }
        }
        
        require_once(ROOT . '/views/admin/index.php');

        
    }

}
