<?php

class SiteController
{
  public function actionIndex() {
    $title = 'Якубівська ЗОШ 1-2 ступенів';
    $albums = Photos::getLastAlbumsId();
    $photos = array();

    foreach($albums as $album) {
      $photos = array_merge($photos, self::getImagesByAlbumId($album['id']));
    }
      
    $news = News::getLastNews();
    require_once(ROOT . '/views/site/index.php');
    return true;
  }

  public function actionView($id) {

    $page = Pages::getPageById($id);
    $title = $page['title'];
    
    require_once(ROOT.'/views/site/view.php');

    if (!isset($_SESSION['watches'])) {
        $_SESSION['watches'] = array();
    }

    if (!in_array("page{$id}", $_SESSION['watches'])) {
        Pages::watch($id, $page['watches']);
        $_SESSION['watches'][] = "page{$id}";
    }
  }

  public function actionContacts() {
    $title = 'Контактні дані';
    require_once(ROOT . '/views/site/contacts.php');
    return true;
  }

  public function action404() {

    require_once(ROOT.'/views/site/404.php');
  }

  private static function getImagesByAlbumId($id) {
    $string = "./uploads/images/albums/{$id}/*.jpg";
    $routes = glob($string);
    $images = array();

    foreach ($routes as $route) {
        $images[] = substr($route, 1);
    }

    return $images;
  }
}
