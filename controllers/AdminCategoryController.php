<?php

/**
 * Контроллер AdminNewsController
 * Управление новостями в админпанели
 */
    class AdminCategoryController extends AdminBase
    {

        /**
         * Action для страницы "Управление новостями"
         */
        public function actionIndex()
        {
            // Проверка доступа
            self::checkAdmin();

            // Получаем список новостей
            $categoryList = Category::getCategoryList();
            $title = 'Управління категоріями';

            // Подключаем вид
            require_once(ROOT . '/views/admin/category/index.php');
            return true;
        }

        /**
         * Action для страницы "Добавить новость"
         */
        public function actionCreate()
        {
            // Проверка доступа
            self::checkAdmin();

            // Обработка формы
            if (isset($_POST['submit'])) {
                // Если форма отправлена
                // Получаем данные из формы
                $options = array();
                $options['name'] = $_POST['name'];
                $options['view_priority'] = $_POST['view_priority'];

                // Флаг ошибок в форме
                $errors = false;

                // При необходимости можно валидировать значения нужным образом
                if (!isset($options['name']) || empty($options['name'])) {
                    $errors[] = 'Заповніть обов\'язкові поля поля';
                }

                if ($errors == false) {
                    // Если ошибок нет
                    // Добавляем новую новость
                    $id = Category::createCategory($options);
                    // Перенаправляем пользователя на страницу управлениями новостями
                    header("Location: /admin/category");
                }
            }
            $title = 'Додати категорію';

            require_once(ROOT . '/views/admin/category/create.php');
            return true;
        }

        /**
         * Action для страницы "Редактировать новость"
         */
        public function actionUpdate($id)
        {
            // Проверка доступа
            self::checkAdmin();

            // Обработка формы
            if (isset($_POST['submit'])) {
                // Если форма отправлена   
                // Получаем данные из формы
                $options = array();
                $options['name'] = $_POST['name'];
                $options['view_priority'] = $_POST['view_priority'];

                // Флаг ошибок в форме
                $errors = false;

                // При необходимости можно валидировать значения нужным образом
                if (!isset($options['name']) || empty($options['name'])) {
                    $errors[] = 'Заповніть обов\'язкові поля поля';
                }

                if ($errors == false) {
                    // Если ошибок нет
                    // Добавляем новую новость
                    $id = Category::updateCategoryById($id, $options);
                    // Перенаправляем пользователя на страницу управлениями новостями
                    header("Location: /admin/category");
                }
            }
            // Получаем данные о конкретной новости
            $category = Category::getCategoryById($id);
            $title = 'Редагувати категорію';
            // Подключаем вид
            require_once(ROOT . '/views/admin/category/update.php');
            return true;
        }


        /**
         * Action для страницы "Удалить новость"
         */
        public function actionDelete($id)
        {
            // Проверка доступа
            self::checkAdmin();

            // Обработка формы
            if (isset($_POST['submit'])) {
                // Если форма отправлена
                // Удаляем новость
                Category::deleteCategoryById($id);
                Pages::disablePagesByCategoryId($id);

                // Перенаправляем пользователя на страницу управлениями новостями
                header("Location: /admin/category");
            }
            $title = 'Видалити категорію сторінок';
            $category = Category::getCategoryById($id);
            // Подключаем вид
            require_once(ROOT . '/views/admin/category/delete.php');
            return true;
        }
    }
