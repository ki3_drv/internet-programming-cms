<?php
  class CollectiveController {

    public function actionAdministration() {
        $title = $branch = 'Адміністрація';
        $people = Collective::getAdmins();

        for ($i = 0; $i < count($people); $i++) {
              $teacher = Collective::getTeacherInfoByPersonId(
                                              $people[$i]['id']);
              $people[$i]['education'] = $teacher['education'];
              $people[$i]['speciality'] = $teacher['speciality'];
              $people[$i]['cathegory'] = $teacher['cathegory'];
        }

        require_once(ROOT.'/views/colective/view.php');
        return true;
    }
    public function actionTeachers() {

        $people = Collective::getTeachersList();

        for ($i = 0; $i < count($people); $i++) {
            $teacher = Collective::getTeacherInfoByPersonId(
                                            $people[$i]['id']);
            $people[$i]['education'] = $teacher['education'];
            $people[$i]['speciality'] = $teacher['speciality'];
            $people[$i]['cathegory'] = $teacher['cathegory'];
        }

        $title = $branch = 'Вчителі';

        require_once(ROOT.'/views/colective/view.php');
        return true;
    }
    public function actionService() {
        $title = $branch = 'Обслуговуючий персонал';
        $people = Collective::getServicePeople();

        require_once(ROOT.'/views/colective/view.php');
        return true;
    }
  }