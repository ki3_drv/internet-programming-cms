<?php

/**
 * Контроллер AdminNewsController
 * Управление новостями в админпанели
 */
class AdminNewsController extends AdminBase
{

    /**
     * Action для страницы "Управление новостями"
     */
    public function actionIndex()
    {
        // Проверка доступа
        self::checkAdmin();

        // Получаем список новостей
        $newsList = News::getNewsListAdmin();

        for($i = 0; $i < count($newsList); $i++) {
            if ($newsList[$i]['album_id'] !== '0') {
                $newsList[$i]['album_title'] =
                         Photos::getAlbumNameById($newsList[$i]['album_id']);
            }
            else {
                $newsList[$i]['album_title'] = '';
            }
        }

        $title = 'Управління новинами';

        // Подключаем вид
        require_once(ROOT . '/views/admin/news/index.php');
        return true;
    }

    /**
     * Action для страницы "Добавить новость"
     */
    public function actionCreate()
    {
        // Проверка доступа
        self::checkAdmin();

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена
            // Получаем данные из формы
            $options = array();
            $options['title'] = $_POST['title'];
            $options['description'] = $_POST['description'];
            $options['album_id'] = $_POST['album_id'];
            $options['status'] = $_POST['status'];

            // Флаг ошибок в форме
            $errors = false;

            // При необходимости можно валидировать значения нужным образом
            if (!isset($options['title']) || empty($options['title']) || 
                !isset($options['description']) || empty($options['description'])) {
                $errors[] = 'Заповніть обов\'язкові поля поля';
            }

            if ($errors == false) {
                // Если ошибок нет
                // Добавляем новую новость
                $options['dateposted'] = date('Y-m-d H:i:s', time());
                $id = News::createNews($options);

                if (isset($id) && is_uploaded_file($_FILES['image']['tmp_name'])) {
                    move_uploaded_file($_FILES['image']['tmp_name'], 
                                        ROOT.'/uploads/images/news/'.$id.'.jpg');
                };

                // Перенаправляем пользователя на страницу управлениями новостями
                header("Location: /admin/news");
            }
        }
        $title = 'Додати новину';
        $albumsList = Photos::getAlbumsList();

        require_once(ROOT . '/views/admin/news/create.php');
        return true;
    }

    /**
     * Action для страницы "Редактировать новость"
     */
    public function actionUpdate($id)
    {
        // Проверка доступа
        self::checkAdmin();

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена   
            // Получаем данные из формы
            $options = array();
            $options['title'] = $_POST['title'];
            $options['description'] = $_POST['description'];
            $options['content'] = $_POST['content'];
            $options['album_id'] = $_POST['album_id'];
            $options['status'] = $_POST['status'];

            // Флаг ошибок в форме
            $errors = false;

            // При необходимости можно валидировать значения нужным образом
            if (!isset($options['title']) || empty($options['title']) || 
                !isset($options['description']) ||
                 empty($options['description']) || 
                !isset($options['content']) || empty($options['content'])) {
                $errors[] = 'Заповніть обов\'язкові поля поля';
            }

            if ($errors == false) {
                // Если ошибок нет
                // Редактируем новость
                News::updateNewsById($id, $options);

                if (is_uploaded_file($_FILES['image']['tmp_name'])) {
                    move_uploaded_file($_FILES['image']['tmp_name'], 
                                        ROOT.'/uploads/images/news/'.$id.'.jpg');
                };

                // Перенаправляем пользователя на страницу управлениями новостями
                header("Location: /admin/news");
            }
        }
        // Получаем данные о конкретной новости
        $news = News::getNewsById($id);
        $title = 'Редагувати новину';
        $albumsList = Photos::getAlbumsList();
        // Подключаем вид
        require_once(ROOT . '/views/admin/news/update.php');
        return true;
    }


    /**
     * Action для страницы "Удалить новость"
     */
    public function actionDelete($id)
    {
        // Проверка доступа
        self::checkAdmin();

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена
            // Удаляем новость
            News::deleteNewsById($id);

            if(file_exists(ROOT."/uploads/images/news/{$id}.jpg")) {
                unlink(ROOT."/uploads/images/news/{$id}.jpg");
            }
                
            // Перенаправляем пользователя на страницу управлениями новостями
            header("Location: /admin/news");
        }
        $title = 'Видалити новину';
        // Подключаем вид
        require_once(ROOT . '/views/admin/news/delete.php');
        return true;
    }

    public function actionDeleteImage($id) {
        if (file_exists("./uploads/images/news/{$id}.jpg")) {
            unlink("./uploads/images/news/{$id}.jpg");
        }

        header("Location: /admin/news/update/{$id}");
    }

}
