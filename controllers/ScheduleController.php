<?php
  class ScheduleController {

    public function actionBells() {
        $title = 'Розклад дзвінків';
        require_once(ROOT.'/views/schedule/bells.php');
        return true;
    }

    public function actionClasses() {
        $scheduleList = Schedule::getScheduleList();
        $title = 'Розклад';
        require_once(ROOT.'/views/schedule/classes.php');
        return true;
    }

    public function actionView($id) {
        $times = ['9:00-9:45', '9:55-10:40', '11:00-11:45',
                        '12:05-12:50', '13:00-13:45', '13:55-14:40',
                        '14:50-15:35', '15:45-16:30'];
        $schedule = Schedule::getScheduleById($id);
        $schedule['schedule'] = json_decode($schedule['schedule_json']);
        $title = 'Розклад для '. $schedule['class'] . ' класу';
        require_once(ROOT.'/views/schedule/view.php');
        return true;
    }

    public function actionAdmission() {
        $title = 'Графік прийому громадян';
        require_once(ROOT.'/views/schedule/admission.php');
        return true;
    }
  }