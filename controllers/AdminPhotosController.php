<?php

/**
 * Контроллер AdminPhotosController
 * Управление фотоальбомами в админпанели
 */
class AdminPhotosController extends AdminBase
{

    /**
     * Action для страницы "Управление фотоальбомами"
     */
    public function actionIndex()
    {
        // Проверка доступа
        self::checkAdmin();

        // Получаем список фотоальбомов
        $albumsList = Photos::getAlbumsListAdmin();
        for ($i = 0; $i < count($albumsList); $i++) {
            $string = "./uploads/images/albums/{$albumsList[$i]['id']}/*.jpg";
            $albumsList[$i]['photos'] = substr_replace(glob($string), '', 0, 1);
        }

        $title = 'Управління фотоальбомами';

        // Подключаем вид
        require_once(ROOT . '/views/admin/photos/index.php');
        return true;
    }

    /**
     * Action для страницы "Добавить фотоальбом"
     */
    public function actionCreate()
    {
        // Проверка доступа
        self::checkAdmin();

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена
            // Получаем данные из формы
            $options = array();
            $options['title'] = $_POST['title'];
            $options['status'] = $_POST['status'];

            // Флаг ошибок в форме
            $errors = false;

            // При необходимости можно валидировать значения нужным образом
            if ( !isset($options['title']) || empty($options['title']) ) {
                $errors[] = 'Заповніть обов\'язкові поля поля';
            }

            if (count($_FILES['image']['name']) < 1) {
                $errors[] = 'Завантажте хоча б одне зображення';
            }

            /*echo "<pre>";
            var_dump($_FILES['image']['name']);
            echo "<pre>";

            die("Nine");*/

            if ($errors == false) {
                // Если ошибок нет
                // Добавляем новую фотоальбом
                $options['dateposted'] = date('Y-m-d H:i:s', time());
                $id = Photos::createAlbum($options);

                if (isset($id)) {
                    mkdir(ROOT.'/uploads/images/albums/'.$id.'/');
                    for($i = 0; $i < count($_FILES['image']['name'][$i]); $i++) {
                        if (is_uploaded_file($_FILES['image']['tmp_name'][$i])) {
                            move_uploaded_file($_FILES['image']['tmp_name'][$i], 
                                ROOT.'/uploads/images/albums/'.$id.'/'.$_FILES['image']['name'][$i]);
                        };
                    }
                }

                // Перенаправляем пользователя на страницу управлениями фотоальбомами
                header("Location: /admin/photos");
            }
        }
        $title = 'Додати новину';
        $albumsList = Photos::getAlbumsList();

        require_once(ROOT . '/views/admin/photos/create.php');
        return true;
    }

    /**
     * Action для страницы "Редактировать фотоальбом"
     */
    public function actionUpdate($id)
    {
        // Проверка доступа
        self::checkAdmin();

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена   
            // Получаем данные из формы
            $options = array();
            $options['title'] = $_POST['title'];
            $options['status'] = $_POST['status'];

            // Флаг ошибок в форме
            $errors = false;

            // При необходимости можно валидировать значения нужным образом
            if (!isset($options['title']) || empty($options['title'])) {
                $errors[] = 'Заповніть обов\'язкові поля поля';
            }

            if ($errors == false) {
                // Если ошибок нет
                // Редактируем фотоальбом
                Photos::updateAlbumById($id, $options);

                //echo count($_FILES['image']['name'][$i]);

                for($i = 0; $i < count($_FILES['image']['name']); $i++) {
                    if (is_uploaded_file($_FILES['image']['tmp_name'][$i])) {
                        //echo "yep";
                        move_uploaded_file($_FILES['image']['tmp_name'][$i], 
                            ROOT.'/uploads/images/albums/'.$id.'/'.$_FILES['image']['name'][$i]);
                    };
                }

                // Перенаправляем пользователя на страницу управлениями фотоальбомами
                header("Location: /admin/photos");
            }
        }
        
        // Получаем данные о конкретного фотоальбома
        $album = Photos::getAlbumById($id);
        $album['photos'] = self::getImagesByAlbumId($id);
        $title = 'Редагувати фотоальбом';
        // Подключаем вид
        require_once(ROOT . '/views/admin/photos/update.php');
        return true;
    }


    public function actionDeleteImage($id, $image) {
        if (file_exists("./uploads/images/albums/{$id}/{$image}")) {
            unlink("./uploads/images/albums/{$id}/{$image}");
        }

        header("Location: /admin/photos/update/{$id}");
    }




    /**
     * Action для страницы "Удалить фотоальбом"
     */
    public function actionDelete($id)
    {
        // Проверка доступа
        self::checkAdmin();

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена
            // Удаляем фотоальбом с базы данных
            Photos::deleteAlbumById($id);

            //Удаляем все фото с дискового пространства
            if ( !empty($files = glob("./uploads/images/albums/{$id}/*")) ) {
                foreach ($files as $file) {
                    unlink($file);
                }
                rmdir("./uploads/images/albums/{$id}");
            }
                
            // Перенаправляем пользователя на страницу управлениями фотоальбомами
            header("Location: /admin/photos");
        }
        $title = 'Видалити фотоальбом';
        // Подключаем вид
        require_once(ROOT . '/views/admin/photos/delete.php');
        return true;
    }



    private static function getImagesByAlbumId($id) {
        $string = "./uploads/images/albums/{$id}/*.jpg";
        $routes = glob($string);
        $images = array();

        foreach ($routes as $route) {
            $tmp_arr = explode('/', $route);
            $images[] = array_pop($tmp_arr);
        }

        return $images;
    }

}
