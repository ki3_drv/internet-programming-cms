<?php
  class PhotosController {
    public function actionIndex() {
      $photos = array();
      $title = 'Фотогалерея';
      $albums = Photos::getAlbumsList();

      foreach($albums as $album) {
        if (!empty(glob("./uploads/images/albums/{$album['id']}/")))
        $photos[$album['id']] = self::getImagesByAlbumId($album['id']);
      }

      require_once(ROOT . '/views/photos/index.php');
      return true;
    }

    public function actionView($id) {
      $album = Photos::getAlbumById($id);
      $title = $album['title'];
      $photos = self::getImagesByAlbumId($id);
      
      require_once(ROOT . '/views/photos/view.php');

      
      if (!isset($_SESSION['watches'])) {
        $_SESSION['watches'] = array();
      }

      if (!in_array("album{$id}", $_SESSION['watches'])) {
          Photos::watch($id, $album['watches']);
          $_SESSION['watches'][] = "album{$id}";
      }

      return true;
    }

    public function actionMore($offset) {
      $photos = array();
      $albums = Photos::getAlbumsList($offset);

      foreach($albums as $album) {
        $photos[$album['id']] = self::getImagesByAlbumId($album['id']);
      }

      require_once(ROOT . '/views/photos/ajax.php');
      return true;
    }

    private static function getImagesByAlbumId($id) {
      $string = "./uploads/images/albums/{$id}/*";
      $routes = glob($string);
      $images = array();

      foreach ($routes as $route) {
          $tmp_arr = explode('/', $route);
          $images[] = array_pop($tmp_arr);
      }

      return $images;
    }
  }