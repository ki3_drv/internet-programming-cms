<?php

/**
 * Контроллер AdminNewsController
 * Управление новостями в админпанели
 */
class AdminLinksController extends AdminBase
{

    /**
     * Action для страницы "Управление новостями"
     */
    public function actionIndex()
    {
        // Проверка доступа
        self::checkAdmin();

        // Получаем список новостей
        $linksList = Links::getLinksListAdmin();
        $title = 'Управління посиланнями';

        // Подключаем вид
        require_once(ROOT . '/views/admin/links/index.php');
        return true;
    }

    /**
     * Action для страницы "Добавить новость"
     */
    public function actionCreate()
    {
        // Проверка доступа
        self::checkAdmin();

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена
            // Получаем данные из формы
            $options = array();
            $options['title'] = $_POST['title'];
            $options['url'] = $_POST['url'];
            $options['status'] = $_POST['status'];

            // Флаг ошибок в форме
            $errors = false;

            // При необходимости можно валидировать значения нужным образом
            if (!isset($options['title']) || empty($options['title']) || 
                !isset($options['url']) || empty($options['url'])) {
                $errors[] = 'Заповніть обов\'язкові поля поля';
            }

            if ($errors == false) {
                // Если ошибок нет
                // Добавляем новую новость
                $id = Links::createLink($options);
                // Перенаправляем пользователя на страницу управлениями новостями
                header("Location: /admin/links");
            }
        }
        $title = 'Додати посилання';

        require_once(ROOT . '/views/admin/links/create.php');
        return true;
    }

    /**
     * Action для страницы "Редактировать новость"
     */
    public function actionUpdate($id)
    {
        // Проверка доступа
        self::checkAdmin();

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена   
            // Получаем данные из формы
            $options = array();
            $options['title'] = $_POST['title'];
            $options['url'] = $_POST['url'];
            $options['status'] = $_POST['status'];

            // Флаг ошибок в форме
            $errors = false;

            // При необходимости можно валидировать значения нужным образом
            if (!isset($options['title']) || empty($options['title']) || 
                !isset($options['url']) || empty($options['url'])) {
                $errors[] = 'Заповніть обов\'язкові поля поля';
            }

            if ($errors == false) {
                // Если ошибок нет
                // Добавляем новую новость
                $id = Links::updateLinkById($id, $options);
                // Перенаправляем пользователя на страницу управлениями новостями
                header("Location: /admin/links");
            }
        }
        // Получаем данные о конкретной новости
        $link = Links::getLinkById($id);
        $title = 'Редагувати посилання';
        // Подключаем вид
        require_once(ROOT . '/views/admin/links/update.php');
        return true;
    }


    /**
     * Action для страницы "Удалить новость"
     */
    public function actionDelete($id)
    {
        // Проверка доступа
        self::checkAdmin();

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена
            // Удаляем новость
            Links::deleteLinkById($id);

            // Перенаправляем пользователя на страницу управлениями новостями
            header("Location: /admin/links");
        }
        
        $title = 'Видалити посилання';
        $link = Links::getLinkById($id);
        // Подключаем вид
        require_once(ROOT . '/views/admin/links/delete.php');
        return true;
    }
}
