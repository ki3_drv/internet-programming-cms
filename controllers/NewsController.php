<?php
  class NewsController {
    public function actionIndex() {
      $title = 'Новини';
      $news = News::getNewsList();
      $news = self::structureNewsList($news);

      require_once(ROOT . '/views/news/index.php');
      return true;
    }

    public function actionView($id) {

      $news = News::getNewsById($id);
      $title = $news['title'];
      $images = self::getImagesByAlbumId($news["album_id"]);

      require_once(ROOT . '/views/news/view.php');


      if (!isset($_SESSION['watches'])) {
          $_SESSION['watches'] = array();
      }

      if (!in_array("news{$id}", $_SESSION['watches'])) {
          News::watch($id, $news['watches']);
          $_SESSION['watches'][] = "news{$id}";
      }
        
      return true;
    }

    public function actionMore($newsOffset) {
      $news = News::getNewsList($newsOffset);
      require_once(ROOT.'/views/news/ajax.php');
      return true;
    }

    private function structureNewsList($news) {
      $structured = array();

      foreach($news as $item) {
        $date = date_parse($item['dateposted']);
        $year = $date['year'];
        $month = $date['month'];
        $structured[$year][$month][] = $item;
      }

      return $structured;

    }

    private static function getImagesByAlbumId($id) {
      $string = "./uploads/images/albums/{$id}/*.jpg";
      $routes = glob($string);
      $images = array();

      foreach ($routes as $route) {
          $tmp_arr = explode('/', $route);
          $images[] = array_pop($tmp_arr);
      }

      return $images;
    }

    private static function getMonthName($month) {
      
    }

    
  }