<?php

/**
 * Контроллер AdminCollectiveController
 * Управление персоналом в админпанели
 */
class AdminCollectiveController extends AdminBase
{

    /**
     * Action для страницы "Управление персоналом"
     */
    public function actionIndex()
    {
        // Проверка доступа
        self::checkAdmin();

        // Получаем список людей
        $peopleList = Collective::getPeopleList();

        for ($i = 0; $i < count($peopleList); $i++) {
            if($peopleList[$i]['is_teacher']) {
                $teacher = Collective::getTeacherInfoByPersonId(
                                                $peopleList[$i]['id']);
                $peopleList[$i]['education'] = $teacher['education'];
                $peopleList[$i]['speciality'] = $teacher['speciality'];
                $peopleList[$i]['cathegory'] = $teacher['cathegory'];
            }
        }

        $title = 'Управління персоналом';

        // Подключаем вид
        require_once(ROOT . '/views/admin/collective/index.php');
        return true;
    }

    /**
     * Action для страницы "Добавить информацию про человека"
     */
    public function actionCreate()
    {
        // Проверка доступа
        self::checkAdmin();

        // Обработка формы
        if (isset($_POST['submit'])) {
            $options = array();
            $teacher = array();
            // Если форма отправлена
            // Получаем данные из формы
            $options['f_name'] = $_POST['f_name'];
            $options['m_name'] = $_POST['m_name'];
            $options['l_name'] = $_POST['l_name'];
            $options['post'] = $_POST['post'];
            $options['is_teacher'] = isset($_POST['is_teacher']);
            $options['biography'] = $_POST['biography'];
            
            if (isset($_POST['is_teacher'])) {
                $options['is_admin'] = isset($_POST['is_admin']);
                $teacher['education'] = $_POST['education'];
                $teacher['speciality'] = $_POST['speciality'];
                $teacher['cathegory'] = $_POST['cathegory'];
            }
            
            $arr = array_merge($options, $teacher);
            // Флаг ошибок в форме
            $errors = false;
            
            foreach ($arr as $key => $value) {
                if (!isset($arr[$key]) || empty($arr[$key])) {
                    if ($key !== 'biography' and $key !== 'is_teacher') {
                        $errors = ['Заповніть обов\'язкові поля'];
                        echo "$key=>$value";
                    }
                }
            }

            if ($errors == false) {
                // Если ошибок нет
                // Добавляем информацию про человека
                $id = Collective::createPersonInfo($options, $teacher);

                if (isset($id) && is_uploaded_file($_FILES['photo']['tmp_name'])) {
                    move_uploaded_file($_FILES['photo']['tmp_name'], 
                                        ROOT.'/uploads/images/collective/'.$id.'.jpg');
                };

                // Перенаправляем пользователя на страницу управлениями персоналом
                header("Location: /admin/collective");
            }
        }
        $title = 'Додати нову інформацію';

        require_once(ROOT . '/views/admin/collective/create.php');
        return true;
    }

    /**
     * Action для страницы "Редактировать информацию"
     */
    public function actionUpdate($id)
    {
        // Проверка доступа
        self::checkAdmin();

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена   
            // Получаем данные из формы
            
            // Флаг ошибок в форме
            $errors = false;

            $options = array();
            $teacher = array();

            $options['f_name'] = $_POST['f_name'];
            $options['m_name'] = $_POST['m_name'];
            $options['l_name'] = $_POST['l_name'];
            $options['post'] = $_POST['post'];
            $options['biography'] = $_POST['biography'];

            if ( !empty(Collective::getTeacherInfoByPersonId($id)) ) {
                $options['is_admin'] = isset($_POST['is_admin']);
                $teacher['education'] = $_POST['education'];
                $teacher['speciality'] = $_POST['speciality'];
                $teacher['cathegory'] = $_POST['cathegory'];
            }

            $arr = array_merge($options, $teacher);

            foreach ($arr as $key => $value) {
                if (!isset($arr[$key]) || empty($arr[$key])) {
                    if ($key !== 'biography' and $key !== 'is_admin') {
                        $errors = ['Заповніть обов\'язкові поля'];
                    }
                }
            }

            // При необходимости можно валидировать значения нужным образом

            if ($errors == false) {
                // Если ошибок нет
                // Редактируем новость
                Collective::updatePersonInfo($id, $options, $teacher);

                if (is_uploaded_file($_FILES['photo']['tmp_name'])) {
                    move_uploaded_file($_FILES['photo']['tmp_name'], 
                                        ROOT.'/uploads/images/collective/'.$id.'.jpg');
                };

                // Перенаправляем пользователя на страницу управлениями персоналом
                header("Location: /admin/collective");
            }
        }
        // Получаем данные о конкретном человеке
        $person = Collective::getPersonById($id);
        $teacher = Collective::getTeacherInfoByPersonId($id);
        $title = 'Редагувати інформацію';
        // Подключаем вид
        require_once(ROOT . '/views/admin/collective/update.php');
        return true;
    }


    /**
     * Action для страницы "Удалить информацию"
     */
    public function actionDelete($id)
    {
        // Проверка доступа
        self::checkAdmin();

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена
            // Удаляем новость
            Collective::deletePersonInfoById($id);

            if(file_exists(ROOT."/uploads/images/collective/{$id}.jpg")) {
                unlink(ROOT."/uploads/images/collective/{$id}.jpg");
            }
                
            // Перенаправляем пользователя на страницу управлениями персоналом
            header("Location: /admin/news");
        }

        $person = Collective::getPersonById($id);
        $title = 'Видалити інформацію';
        // Подключаем вид
        require_once(ROOT . '/views/admin/collective/delete.php');
        return true;
    }

    public function actionDeleteImage($id) {
        if (file_exists("./uploads/images/collective/{$id}.jpg")) {
            unlink("./uploads/images/collective/{$id}.jpg");
        }

        header("Location: /admin/collective/update/{$id}");
    }

}
