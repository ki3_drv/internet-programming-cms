<?php
    class AdminScheduleController extends AdminBase {
        public static $week = array(
                'Mon' => 'Понеділок',
                'Tus' => 'Вівторок', 
                'Wen' => 'Середа', 
                'Thu' => 'Четвер', 
                'Fri' => 'П\'ятниця'
            );

        public function actionIndex() {
            $title = 'Управління розкладом уроків';

            $scheduleList = Schedule::getScheduleListAdmin();

            for($i = 0; $i < count($scheduleList); $i++) {
                $scheduleList[$i]['schedule'] =
                         json_decode($scheduleList[$i]['schedule_json']);
            }

            
            /*echo '<pre>';
            var_dump($scheduleList[0]['schedule']);
            echo '<pre>';*/
            require_once(ROOT.'/views/admin/schedule/index.php');
        }

        public function actionCreate() {
            
            if (isset($_POST['submit'])) {
                $options['class'] = $_POST['class'];
                $options['status'] = $_POST['status'];

                $schedule_arr = array();

                foreach(self::$week as $key => $val) {
                    $schedule_arr[$key] = array();

                    for($i = 1; $i < 9; $i++) {
                        $schedule_arr[$key][$i] = $_POST["{$key}_{$i}"];
                    }
                }

                $options['schedule_json'] = json_encode($schedule_arr);
                

                /*echo '<pre>';
                var_dump($options['schedule_json']);
                echo '<pre>';*/

                $errors = false;

                if (!isset($options['class']) || empty($options['class']))
                    $errors[] = 'Заповніть обов\'язкові поля';

                if (Schedule::classExists($options['class']))
                    $errors[] = 'Розклад для цього класу уже існує, 
                    перейдіть до Управління розкладом та редагуйте його при необхідності';

                //var_dump($errors);
                if ($errors === false) {

                    Schedule::createSchedule($options);

                    header('Location: admin/schedule');
                }

                
            }
            
            
            
            $title = 'Додати розклад';
            require_once(ROOT.'/views/admin/schedule/create.php');
            return true;
        }
        
        public function actionUpdate($id) {
            
            $schedule = Schedule::getScheduleById($id);
            
            
            if (isset($_POST['submit'])) {
                $options['class'] = $_POST['class'];
                $options['status'] = $_POST['status'];
                
                $schedule_arr = array();
                
                foreach(self::$week as $key => $val) {
                    $schedule_arr[$key] = array();
                    
                    for($i = 1; $i < 9; $i++) {
                        $schedule_arr[$key][$i] = $_POST["{$key}_{$i}"];
                    }
                }
                
                $options['schedule_json'] = json_encode($schedule_arr);
                
                $errors = false;
                
                if (!isset($options['class']) || empty($options['class']))
                    $errors[] = 'Заповніть обов\'язкові поля';
                

                if($options['class'] !== $schedule['class'] &&
                    Schedule::classExists($options['class'])) {
                    $errors[] = 'Розклад для цього класу уже існує, 
                            перейдіть до Управління розкладом та редагуйте його при необхідності';
                }
                
                //var_dump($errors);
                if ($errors === false) {
                    Schedule::updateScheduleById($id, $options);
                    
                    header('Location: admin/schedule');
                }
            }
            $schedule['schedule'] = 
                            json_decode($schedule['schedule_json']);
            $title = "Обновити розклад для {$schedule['class']} класу";

            require_once(ROOT.'/views/admin/schedule/update.php');
            return true;
        }

        public function actionDelete($id) {

            if(isset($_GET['submit'])) {
                Schedule::deleteScheduleById($id);

                header('Location: admin/schedule');
            }

            $schedule = Schedule::getScheduleById($id);
            $title = "Видалити розклад";

            require_once(ROOT.'/views/admin/schedule/delete.php');
        }
        



    }