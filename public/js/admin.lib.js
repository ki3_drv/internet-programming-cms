"use strict"

function toggleTeachrInfo(e) {
  let checkbox = document.querySelector(".teacher-info");
  checkbox.classList.toggle('hidden');
}

let preview = document.querySelector(".preview");
var counter = 0;

function bindEvent() {
  document.querySelector("input.img")
      .addEventListener("change", handleFiles, false);
  preview.innerHTML = "";
}


function handleFiles() {
  const fileList = this.files;

  for(let i = 0; i < fileList.length; i++) {
    const file = fileList[i];

    if(!file.type.startsWith('image/')) {
      continue;
    }

    const img = document.createElement("img");
    img.classList.add("img");
    img.file = file;

    preview.appendChild(img);

    const reader = new FileReader();
    reader.onload = (function(aImg) {
      return function(e) {
        aImg.src = e.target.result;
      };
    })(img);
    reader.readAsDataURL(file);
  }
}