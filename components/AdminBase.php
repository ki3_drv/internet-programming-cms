<?php

/**
 * Абстрактный класс AdminBase содержит общую логику для контроллеров, которые 
 * используются в панели администратора
 */
abstract class AdminBase
{

    /**
     * Метод, который проверяет пользователя на то, является ли он администратором
     * @return boolean
     */
    public static function checkAdmin()
    {
       
        // Если роль текущего пользователя "admin", пускаем его в админпанель
        if (isset($_SESSION['admin'])) {
            return true;
        }

        header('/admin');
    }

}
