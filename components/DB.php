<?php

class DB
{
  protected $Connection;

  public static function getConnection() {
    $paramsPath = ROOT . '/config/db_params.php';
    $params = include($paramsPath);

    $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";

    $db = new PDO($dsn, $params['user'], $params['password'], 
                        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

    return $db;
  }
  //params[] - list of params
  public static function Select($table, $row = null, $fields = "*", $params = null) {
    $whereString = '';
    $orderString = '';
    $paramsString = '';
    $valuesList = null;

    if(!empty($row)) {
      $fieldsList = array_keys($row);
      $valuesList = array_values($row);
      $arr = array();

      foreach ($fieldsList as $field) {
        array_push($arr, "{$field} = ?");
      }

      $whereString = "WHERE " . implode(' AND ', $arr);
    }

    if (!empty($params)) {
      if(isset($params['order']))
        $paramsString .= "ORDER BY {$params['order']}";
      
      if(isset($params['limit'])) 
          $paramsString .= " LIMIT {$params['limit']}";
    }

    $sql = "SELECT {$fields} FROM {$table} {$whereString} {$paramsString}";
    $db = self::getConnection();
    $result = $db->prepare($sql);
    $result->execute($valuesList);

    return $result->fetchAll(PDO::FETCH_ASSOC);
  }

  public static function Create($table, $row) {
    $fieldsList = array_keys($row);
    $valuesList = array_values($row);
    $fieldsString = implode($fieldsList, ', ');
    $params = array();

    for($i = 0; $i < count($fieldsList); $i++)
        array_push($params, '?');

    $paramsString = implode($params, ', ');
    $sql = "INSERT INTO {$table}({$fieldsString}) VALUES({$paramsString})";



    $db = self::getConnection();
    $result = $db->prepare($sql);
    $result->execute($valuesList);

    return $db->lastInsertId();
  }

  public static function Update($table, $fields, $row = null) {
    /**UPDATE table SET field=value, field=value,.. WHERE  */
    $whereString = '';
    $rowValuesList = null;

    if (!empty($row)) {
        $rowFieldsList = array_keys($row);
        $rowValuesList = array_values($row);
        $arr = array();

        foreach($rowFieldsList as $field)
            array_push($arr, "{$field} = ?");
        
        $whereString = "WHERE " . implode($arr, ' AND ');
    }

    $setFieldsList = array_keys($fields);
    $setValuesList = array_values($fields);
    $arr = array();

    foreach($setFieldsList as $field)
        array_push($arr, "{$field} = ?");
    
    $settingString = "SET " . implode($arr, ', ');
    $sql = "UPDATE {$table} {$settingString} {$whereString}";

    $db = DB::getConnection();

    $result = $db->prepare($sql);
    $result->execute(array_merge($setValuesList, $rowValuesList));
    return $db->lastInsertId();
  }

  public static function Delete($table, $row = null) {
    $whereString = '';
    $valuesList = null;

    if(!empty($row)) {
        $fieldsList = array_keys($row);
        $valuesList = array_values($row);
        $arr = array();

        foreach($fieldsList as $field)
            array_push($arr, "{$field} = ?");

        $whereString = 'WHERE ' . implode($arr, ' AND ');
    }

    $sql = "DELETE FROM {$table} {$whereString} ";
    $db = self::getConnection();
    $result = $db->prepare($sql);
    $result->execute($valuesList);
  }

}