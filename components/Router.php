<?php

  class Router
  {
    private $routes;

    public function __construct()
    {
      $routesPath = ROOT . '/config/routes.php';
      $this->routes = include($routesPath);
    }


    /**
     *  Returns request string
     * @return string
     */
    private function getURI()
    {
      if (!empty($_SERVER['REQUEST_URI']))
        return trim($_SERVER['REQUEST_URI'], '/');
    }

    public function run()
    {
       //Get the uri
      
      $uri = $this->getURI();
      
      foreach ($this->routes as $uriPattern => $path) {
        //echo $uriPattern . '<br>' . $uri .'<br>';
        if (preg_match("~$uriPattern~", $uri)) {
          $internalRoute = preg_replace("~$uriPattern~", $path, $uri);
          //echo $internalRoute, '<br>';
          $segments = explode('/', $internalRoute);

          $controllerName = ucfirst(array_shift($segments)).'Controller';

          $actionName = 'action' . ucfirst(array_shift($segments));
          $parameters = $segments;

          /*echo '<pre>';
          print_r($parameters);
          echo '<pre>';
          echo '<br>', $actionName, '<br>', $controllerName, '<br>';*/
          
          $controllerFile = ROOT . '/controllers/' . 
                  $controllerName . '.php';

          if (file_exists($controllerFile))
            include_once($controllerFile);

          $controllerObject = new $controllerName();

          $result = call_user_func_array(array($controllerObject, $actionName), $parameters);
          
          if ($result != null) {
            break;
          }
        }
      }
    }
    
  }