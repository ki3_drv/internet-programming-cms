<?php
  return array(

    'admin/news/create' => 'adminNews/create',
    'admin/news/delete/(\d+)' => 'adminNews/delete/$1',
    'admin/news/delete-image/(\d+)' => 'adminNews/deleteImage/$1',
    'admin/news/update/(\d+)' => 'adminNews/update/$1',
    'admin/news' => 'adminNews/index',

    'admin/collective/create' => 'adminCollective/create',
    'admin/collective/delete/(\d+)' => 'adminCollective/delete/$1',
    'admin/collective/delete-image/(\d+)' => 'adminCollective/deleteImage/$1',
    'admin/collective/update/(\d+)' => 'adminCollective/update/$1',
    'admin/collective' => 'adminCollective/index',

    'admin/photos/create' => 'adminPhotos/create',
    'admin/photos/delete/(\d+)' => 'adminPhotos/delete/$1',
    'admin/photos/delete-image/(\d+)/(.+)' => 'adminPhotos/deleteImage/$1/$2',
    'admin/photos/update/(\d+)' => 'adminPhotos/update/$1',
    'admin/photos' => 'adminPhotos/index',

    'admin/pages/create' => 'adminPages/create',
    'admin/pages/delete/(\d+)' => 'adminPages/delete/$1',
    'admin/pages/delete-image/(\d+)' => 'adminPages/deleteImage/$1',
    'admin/pages/update/(\d+)' => 'adminPages/update/$1',
    'admin/pages' => 'adminPages/index',
    
    'admin/schedule/create' => 'adminSchedule/create',
    'admin/schedule/delete/(\d+)' => 'adminSchedule/delete/$1',
    'admin/schedule/update/(\d+)' => 'adminSchedule/update/$1',
    'admin/schedule' => 'adminSchedule/index',
    
    'admin/category/create' => 'adminCategory/create',
    'admin/category/delete/(\d+)' => 'adminCategory/delete/$1',
    'admin/category/update/(\d+)' => 'adminCategory/update/$1',
    'admin/category' => 'adminCategory/index',

    'admin/links/create' => 'adminLinks/create',
    'admin/links/delete/(\d+)' => 'adminLinks/delete/$1',
    'admin/links/update/(\d+)' => 'adminLinks/update/$1',
    'admin/links' => 'adminLinks/index',

    
    'news/view/(\d+)' => 'news/view/$1',
    'news/more/(\d+)' => 'news/more/$1',
    'news' => 'news/index',
    
    'photos/view/(\d+)' => 'photos/view/$1',
    'photos/more/(\d+)' => 'photos/more/$1',
    'photos' => 'photos/index',
    
    'collective/admins' => 'collective/administration',
    'collective/teachers' => 'collective/teachers',
    'collective/service' => 'collective/service',
    
    'schedule/bells' => 'schedule/bells',
    'schedule/classes/(\d)' => 'schedule/view/$1',
    'schedule/classes' => 'schedule/classes',
    'schedule/facultative' => 'schedule/facultative',
    'schedule/admission' => 'schedule/admission',
    
    'admin/login' => 'admin/login',
    'admin' => 'admin/index',
    
    'page/view/(\d+)' => 'site/view/$1',
    'contacts' => 'site/contacts',
    'history' => 'site/history',
    'visitka' => 'site/visitka',

    '' => 'site/index',

  );